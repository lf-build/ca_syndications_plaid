﻿using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.EventHub;
using LendFoundry.Syndication.Plaid.Abstractions;
using CapitalAlliance.Cashflow.Client;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Syndication.Plaid.Persistence;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
namespace LendFoundry.Syndication.Plaid.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Plaid"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Syndication.Plaid.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerDocumentation();
#endif
            services.AddTenantTime();

            services.AddTokenHandler();

            services.AddHttpServiceLogging(Settings.ServiceName);

            services.AddConfigurationService<Configuration>(
                Settings.Configuration.Host, 
                Settings.Configuration.Port, 
                Settings.ServiceName);

            services.AddTransient<IConfiguration>(p =>
            {
                var configuration = p.GetService<IConfigurationService<Configuration>>().Get();
                //configuration.ProxyUrl = $"http://{Settings.TlsProxy.Host}:{Settings.TlsProxy.Port}";
                return configuration;
            });
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            //services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddCashflowService(Settings.Cashflow.Host, Settings.Cashflow.Port);
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);           
            services.AddTransient<IClientTrackRepository, ClientTrackRepository>();      

            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            // internals
            services.AddProjectDependencies();

            // aspnet mvc related
            services.AddMvc()
                .AddLendFoundryJsonOptions();
            services.AddCors();
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Plaid Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            app.UseHealthCheck();
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UsePlaidListener();
           
        }
    }
}
