﻿using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Syndication.Plaid.Persistence;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Persistence.Mongo;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Syndication.Plaid.Api
{
    public static class APIServiceCollectionExtensions
    {
        public static IServiceCollection AddProjectDependencies(
            this IServiceCollection services, string description = null
            )
        {
            services.AddSingleton<IMongoConfiguration>(
                p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IConfiguration>(
                provider => provider.GetRequiredService<IConfigurationService<LendFoundry.Syndication.Plaid.Configuration>>().Get());

            //// Repository
            services.AddTransient<IInstitutionsRepository, InstitutionsRepository>();
            services.AddTransient<IFeatureInstitutionsRepository, FeatureInstitutionsRepository>();
            services.AddTransient<IItemRepository, ItemRepository>();
            services.AddTransient<ITransactionRepository, TransactionRepository>();
            services.AddTransient<IWebhookRepository, WebhookRepository>();
            services.AddTransient<IPlaidDelinkRepository, PlaidDelinkRepository>();

            //// Service
            services.AddTransient<IInstitutionService, InstitutionService>();
            services.AddTransient<IItemService, ItemService>();
            services.AddTransient<IPlaidClient, PlaidClient>();
            services.AddTransient<ITransactionService, TransactionService>();
            services.AddTransient<IFilterTransactionService, FilterTransactionService>();

            services.AddTransient<IItemServiceFactory, ItemServiceFactory>();
            services.AddTransient<IRepositoryFactory, RepositoryFactory>();
            services.AddTransient<IPlaidClientFactory, PlaidClientFactory>();

            services.AddTransient<IPlaidListener, PlaidListener>();            

            return services;
        }
    }
}
