﻿using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Plaid.Abstractions;
namespace LendFoundry.Syndication.Plaid.Api.Controllers
{
    public class CashFlowController : ExtendedController
    {
        public CashFlowController(
            ITransactionService transactionService,
            IFilterTransactionService filterTransactionService
            )
        {
            this.TransactionService = transactionService;
            this.FilterTransactionService = filterTransactionService;
        }

        /// <summary>
        /// Gets TransactionService
        /// </summary>
        private ITransactionService TransactionService { get; }

        /// <summary>
        /// FilterTransactionService
        /// </summary>
        private IFilterTransactionService FilterTransactionService { get; }

        // Fetch from DB
        /// <summary>
        /// GetTransactionData
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("plaidv2/transaction/get")]

        [ProducesResponseType(typeof(IEnumerable<ITransaction>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public Task<IActionResult> GetTransactions([FromBody]RequestTransaction request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.TransactionService.TransactionData(request));
            });
        }

        [HttpPost]
        [Route("plaidv2/transaction/pull")]

        [ProducesResponseType(typeof(LendFoundry.Syndication.Plaid.TransactionResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public Task<IActionResult> GetTransactionPull([FromBody]RequestTransaction request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.TransactionService.PullTransactionAsync(request));
            });
        }

        [HttpPost]
        [HttpGet]
        [Route("plaidv2/transaction/duration/{accountId}")]

        [ProducesResponseType(typeof(IList<DateTime>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        
        public Task<IActionResult> GetTransactionDuration(string accountId)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.TransactionService.TransactionDurationDates(accountId));
            });
        }

        [HttpPost]
        [Route("plaidv2/filtertransaction")]

        [ProducesResponseType(typeof(IList<ITransaction>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        
        public Task<IActionResult> FilterTransaction([FromBody]FilterTransactionRequest request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.FilterTransactionService.GetFilterTransactions(request));
            });
        }
    }
}
