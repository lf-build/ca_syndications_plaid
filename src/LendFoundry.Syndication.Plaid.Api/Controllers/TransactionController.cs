﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Plaid.Abstractions;
using System.Collections.Generic;
using System;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.Syndication.Plaid.Api.Controllers
{
    public class TransactionController : ExtendedController
    {
        public TransactionController(
            ITransactionService transactionService,
            IFilterTransactionService filterTransactionService
            )
        {
            this.TransactionService = transactionService;
            this.FilterTransactionService = filterTransactionService;
        }

        /// <summary>
        /// Gets TransactionService
        /// </summary>
        private ITransactionService TransactionService { get; }

        /// <summary>
        /// FilterTransactionService
        /// </summary>
        private IFilterTransactionService FilterTransactionService { get; }

        // Fetch from DB
        /// <summary>
        /// GetTransactionData
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("transaction/get")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<ITransaction>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> GetTransactions([FromBody]RequestTransaction request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.TransactionService.TransactionData(request));
            });
        }

        [HttpPost]
        [Route("transaction/pull")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Syndication.Plaid.TransactionResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> GetTransactionPull([FromBody]RequestTransaction request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.TransactionService.PullTransactionAsync(request));
            });
        }
       
        [HttpPost]
        [HttpGet]
        [Route("transaction/duration/{accountId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IList<DateTime>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> GetTransactionDuration(string accountId)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.TransactionService.TransactionDurationDates(accountId));
            });
        }

        [HttpPost]
        [Route("filtertransaction")]
#if DOTNET2
        [ProducesResponseType(typeof(IList<ITransaction>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> FilterTransaction([FromBody]FilterTransactionRequest request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.FilterTransactionService.GetFilterTransactions(request));
            });
        }
    }
}
