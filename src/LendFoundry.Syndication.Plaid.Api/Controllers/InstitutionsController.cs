﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using System.Web;
#else
using Microsoft.AspNet.Mvc;
using RestSharp.Contrib;
#endif
namespace LendFoundry.Syndication.Plaid.Api.Controllers
{
    public class InstitutionsController : ExtendedController
    {
        public InstitutionsController(            
            IInstitutionService institutionService,
            ILogger log)
        {
            this.InstitutionService = institutionService;
            this.log = log;
        }
           
        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets Institution Service
        /// </summary>
        private IInstitutionService InstitutionService { get; }

        private ILogger log { get; }

        [HttpPost]
        [HttpGet]
        [Route("institutions/get/{count}/{offset}")]
#if DOTNET2
        [ProducesResponseType(typeof(IList<InstitutionsVM>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> GetInstitutions(int count, int offset)
        {
            this.log.Info("Started GetInstitutions...");
            return ExecuteAsync(async () =>
            {
                var result = await this.InstitutionService.InstitutionsGetAsync(count, offset);
                this.log.Info("...Completed GetInstitutions");
                return this.Ok(result);
            });
        }

        [HttpPost]
        [HttpGet]
        [Route("institutions/search/{searchkey}")]
#if DOTNET2
        [ProducesResponseType(typeof(IList<InstitutionsSearchVM>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> GetInstitutions([FromRoute]string searchkey,[FromQuery]bool image)
        {
            this.log.Info("Started GetInstitutions...");
            return ExecuteAsync(async () =>
            {
                var result = await this.InstitutionService.InstitutionsSearchAsync(HttpUtility.UrlDecode(searchkey));
                this.log.Info("...Completed GetInstitutions");
                return this.Ok(result);
            });
        }

        [HttpPost]
        [HttpGet]
        [Route("institutions/detail/{institutionId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IList<InstitutionsSearchVM>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> GetInstitutionsWithDetails(string institutionId)
        {
            this.log.Info("Started GetInstitutionsWithDetails...");
            return ExecuteAsync(async () =>
            {
                var result = await this.InstitutionService.InstitutionsDetailsAsync(institutionId);
                this.log.Info("...Completed GetInstitutionsWithDetails");
                return Ok(result);
            });
        }

        [HttpPost]
        [HttpGet]
        [Route("institutions/search/list")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Syndication.Plaid.InstitutionsVM), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> GetSearchInstitutionList()
        {
            this.log.Info("Started GetSearchInstitutionList...");
            return ExecuteAsync(async () =>
            {
                var result = await this.InstitutionService.InstitutionsSearchListAsync();
                this.log.Info("...Completed GetSearchInstitutionList");
                return Ok(result);
            });
        }

        [HttpPost]
        [HttpGet]
        [Route("institutions/feature")]
#if DOTNET2
        [ProducesResponseType(typeof(IList<InstitutionsFeatureVM>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> GetFeatureInstitutions()
        {
            this.log.Info("Started GetFeatureInstitutions...");
            return ExecuteAsync(async () =>
            {
                var result = await InstitutionService.GetFeatureInstitutionsAsync();
                this.log.Info("...Completed GetFeatureInstitutions");
                return Ok(result);
            });
        }
    }
}
