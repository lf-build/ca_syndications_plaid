﻿using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
namespace LendFoundry.Syndication.Plaid.Api.Controllers
{
    public class DataSyncController : ExtendedController
    {
        public DataSyncController(
            IDataSyncService dataSyncService)
        {
            this.DataSyncService = dataSyncService;
        }
       
        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets Institution Service
        /// </summary>
        private IDataSyncService DataSyncService { get; }

        [HttpPost]
        [Route("syncinstitutions")]
#if DOTNET2
        [ProducesResponseType (typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif    
        public Task<IActionResult> SyncInstitutions()
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.DataSyncService.SyncInstitutionsAsync());
            });
        }

        [HttpPost]
        [Route("updateinstitutions")]
#if DOTNET2
        [ProducesResponseType (typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> UpdateInstitutions([FromQuery]string institutionId)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.DataSyncService.SyncInstitutionsByIdAsync(institutionId));
            });
        }        
    }
}
