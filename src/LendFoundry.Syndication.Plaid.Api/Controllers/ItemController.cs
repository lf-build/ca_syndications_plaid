﻿using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
#if DOTNET2
using System.Web;
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
using RestSharp.Contrib;
#endif
namespace LendFoundry.Syndication.Plaid.Api.Controllers
{
    public class ItemController : ExtendedController
    {
        public ItemController(IItemService itemService, ILogger logger)
        {
            this.ItemService = itemService;
            this.Log = logger;
        }

        /// <summary>
        /// Get ItemService 
        /// </summary>
        private IItemService ItemService { get;}

        private ILogger Log { get; }

        //// Step 1 : for creating item and check the response if it is device type or selections or Direct
        [HttpPost]
        [Route("item/create")]
 #if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Syndication.Plaid.IResponseItemCreate), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> CreateItem([FromBody]RequestItemCreate request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.ItemService.CreateItemAsync(request));
            });
        }

        //// for creating item and check the response if it is device type or selections or Direct
        [HttpPost]
        [Route("item/mfa")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Syndication.Plaid.IResponseItemCreate), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> MfaRequest([FromBody]MFADocittRequest request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.ItemService.ItemCreateMFAAsync(request));
            });
        }

        [HttpPost]
        [Route("item/delete")]
#if DOTNET2
        [ProducesResponseType(typeof(IList<PlaidResponseItemDelete>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> DeleteItem([FromBody]RequestItemDelete request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.ItemService.ItemDeleteAsync(request));
            });
        }

        [HttpPost]
        [Route("item/account/delete")]
#if DOTNET2
         [ProducesResponseType(typeof(bool), 200)]
         [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> DeleteItemAccount([FromBody]RequestItemDelete request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.ItemService.ItemAccountDeleteAsync(request));
            });
        }

        [HttpPost]
        [HttpGet]
        [Route("item/accounts/all/{applicantId}")]
#if DOTNET2
         [ProducesResponseType(typeof(IList<AccountsVM>), 200)]
         [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> GetItemAccount(string applicantId)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.ItemService.GetItemsWithAccounts(HttpUtility.UrlDecode(applicantId)));
            });
        }

        [HttpPut]
        [Route("item/accounts/sync")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Syndication.Plaid.IAccountSyncVM), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif       
        public Task<IActionResult> UpdateAccountSync([FromBody]RequestItemAccount request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.ItemService.GetAccountAsync(request));
            });
        }

        [HttpPost]
        [Route("item/delink/{days}")]
#if DOTNET2
        [ProducesResponseType(typeof(List<string>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> DelinkItems(int days)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.ItemService.DelinkItems(days));
            });
        }
        [HttpPost]
        [Route("item/deletetoken")]
#if DOTNET2
        [ProducesResponseType(typeof(List<string>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> DeleteToken()
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.ItemService.DeleteToken());
            });
        }
        [HttpPost]
        [Route("item/update")]
#if DOTNET2
        [ProducesResponseType(typeof(LendFoundry.Syndication.Plaid.IResponseItemCreate), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif        
        public Task<IActionResult> UpdateItem([FromBody]RequestItemUpdate request)
        {
            return ExecuteAsync(async () =>
            {
                return this.Ok(await this.ItemService.ItemUpdateAsync(request));
            });
        }
    }
}
