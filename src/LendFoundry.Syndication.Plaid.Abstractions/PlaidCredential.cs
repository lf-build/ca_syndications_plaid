﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{
    public class PlaidCredential : IPlaidCredential
    {
        public string client_id { get; set; }

        public string secret { get; set; }

        public int count { get; set; }

        public int offset { get; set; }
    }
}


