﻿using System.Collections.Generic;
using LendFoundry.Syndication.Plaid.Abstractions;

namespace LendFoundry.Syndication.Plaid {
    public class Configuration : IConfiguration {
        public EventMapping[] Events { get; set; }

        public string APIURL { get; set; }
        public string ClientId { get; set; }
        public bool IsLive { get; set; }
        public string Secret { get; set; }
        public string TLSVersion { get; set; }
        public int TransactionDays { get; set; }
        public bool UseProxy { get; set; }
        public string ProxyUrl { get; set; }
        public List<EventConfiguration> events { get; set; }
        public bool PublishBankLinkEvent { get; set; }
        public bool SaveCashflowReport { get; set; }
        public Dictionary<string, string> DocumentCategory { get; set; }
        public string ApplicationDocumentEvent { get; set; }
        public string SimulationUrl { get; set; }
        public bool UseSimulation { get; set; }
        public string clientName { get; set; }
        public string env { get; set; }
        public string key { get; set; }
        public List<string> product { get; set; }
        public int TransactionBatchCount { get; set; }
        public bool EnableBulkTransaction { get; set; }
        public int AccountBatchCount { get; set; }
        public bool EnableBulkAccount { get; set; }
        public int WaitDurationForTransaction { get; set; }
        public bool AsyncTransactionStep { get; set; }
        public string PublicKey { get; set; }
        public string UrlSandbox { get; set; }
        public string WebhookUrl { get; set; }
        public string DeleteTokenRule { get; set; }
        public string SearchFilterURL { get; set; }
        public string SavedSearchName { get; set; }
        public string[] InitialProducts { get; set; }
        public int CacheSlidingExpiration { get; set; }
        public int PageCount { get; set; }

        public int MaxRetry { get; set; }
    }
}