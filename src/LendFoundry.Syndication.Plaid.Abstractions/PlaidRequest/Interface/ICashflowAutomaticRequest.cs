﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{
    public interface ICashflowAutomaticRequest
    {
        string PublicToken { get; set; }
        string BankSupportedProductType { get; set; }
        string AccountId { get; set; }
        string EntityId { get; set; }
        string EntityType { get; set; }
        string InstitutionId { get; set; }
        string InstitutionName { get; set; }
    }
}
