﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CapitalAlliance.Cashflow;
namespace LendFoundry.Syndication.Plaid
{
    public interface IBankRequestAccounts
    {
        List<IBankAccount> Accounts { get; set; }
    }
}
