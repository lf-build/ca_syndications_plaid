﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{
    public interface IPlaidRequestItemUpdate
    {
        PlaidUserCredential Credentials { get; set; }

        string PublicToken { get; set; }

        string Public_Key { get; set; }
    }
}
