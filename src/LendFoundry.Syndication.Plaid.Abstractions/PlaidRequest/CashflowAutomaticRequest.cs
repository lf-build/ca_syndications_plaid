﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Plaid
{
    public class CashflowAutomaticRequest : ICashflowAutomaticRequest
    {
        #region Public Constructors

        public CashflowAutomaticRequest()
        {
        }

        public CashflowAutomaticRequest(ICashflowAutomaticRequest request)
        {
            PublicToken = request.PublicToken;
            BankSupportedProductType = request.BankSupportedProductType;
            AccountId = request.AccountId;
            InstitutionId = request.InstitutionId;
            InstitutionName = request.InstitutionName;
        }

        #endregion Public Constructors

        #region Public Properties

        public string PublicToken { get; set; }

        public string BankSupportedProductType { get; set; }

        public string AccountId { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }

        public string InstitutionId { get; set; }
        public string InstitutionName { get; set; }

        #endregion Public Properties
    }
}



