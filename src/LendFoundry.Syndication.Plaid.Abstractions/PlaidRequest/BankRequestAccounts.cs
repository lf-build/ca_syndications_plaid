﻿
using LendFoundry.Syndication.Plaid;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CapitalAlliance.Cashflow;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Syndication.Plaid
{
    public class BankRequestAccounts : IBankRequestAccounts
    {
        public BankRequestAccounts() { }

        [JsonConverter(typeof(InterfaceListConverter<IBankAccount, BankAccount>))]
        public List<IBankAccount> Accounts { get; set; }

    }
}








