﻿using LendFoundry.Syndication.Plaid.Abstractions;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Plaid
{
    public class EventConfiguration : IEventConfiguration
    {
        public string EntityId { get; set; }
        public string Response { get; set; }
        public string EntityType { get; set; }
        public string Name { get; set; }
        public string MethodToExecute { get; set; }
        public string ResponseObject { get; set; }       
        public List<string> CompletionEvents { get; set; }
        public string Rule { get; set; }
    }
}
