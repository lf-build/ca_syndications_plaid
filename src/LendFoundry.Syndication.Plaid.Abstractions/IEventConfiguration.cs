﻿using LendFoundry.Syndication.Plaid.Abstractions;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Plaid
{
    public interface IEventConfiguration
    {
        string EntityId { get; set; }        
        string EntityType { get; set; }
        string Name { get; set; }
        string MethodToExecute { get; set; }
        string ResponseObject { get; set; }
        string Response { get; set; }      
        List<string> CompletionEvents { get; set; }
        string Rule { get; set; }
    }
}
