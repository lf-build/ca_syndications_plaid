﻿
using LendFoundry.SyndicationStore.Events;
namespace LendFoundry.Syndication.Plaid.Abstractions.Events
{
    public class PlaidSaveProductTypeRequestedFail : SyndicationCalledEvent
    {
    }
}
