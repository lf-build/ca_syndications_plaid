﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions {
    public interface IConfiguration {
        EventMapping[] Events { get; set; }

        string APIURL { get; set; }
        string ClientId { get; set; }
        bool IsLive { get; set; }
        string Secret { get; set; }
        string TLSVersion { get; set; }
        int TransactionDays { get; set; }
        bool UseProxy { get; set; }
        string ProxyUrl { get; set; }
        List<EventConfiguration> events { get; set; }
        bool PublishBankLinkEvent { get; set; }
        bool SaveCashflowReport { get; set; }
        Dictionary<string, string> DocumentCategory { get; set; }
        string ApplicationDocumentEvent { get; set; }
        string SimulationUrl { get; set; }
        bool UseSimulation { get; set; }
        string clientName { get; set; }
        string env { get; set; }
        string key { get; set; }
        List<string> product { get; set; }
        int TransactionBatchCount { get; set; }
        bool EnableBulkTransaction { get; set; }
        int AccountBatchCount { get; set; }
        bool EnableBulkAccount { get; set; }
        int WaitDurationForTransaction { get; set; }
        bool AsyncTransactionStep { get; set; }
        string PublicKey { get; set; }
        string UrlSandbox { get; set; }
        string WebhookUrl { get; set; }
        string DeleteTokenRule { get; set; }
        string SearchFilterURL { get; set; }
        string SavedSearchName { get; set; }
        string[] InitialProducts { get; set; }
        int CacheSlidingExpiration { get; set; }
        int PageCount { get; set; }
        int MaxRetry { get; set; }
    }
}