﻿namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IPlaidListener
    {
        void Start();
    }
}
