﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// IGetTransactionService
    /// </summary>
    public interface ITransactionService
    {
        Task<IEnumerable<ITransaction>> TransactionData(IRequestTransaction transactionRequest);

        Task<TransactionResponse> PullTransactionAsync(IRequestTransaction transactionRequest);

        Task<IList<DateTime>> TransactionDurationDates(string accountId);
        
        string GetAccessTokenByItemId(string itemId);
    }
}
