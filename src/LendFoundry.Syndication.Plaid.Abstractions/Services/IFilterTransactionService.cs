﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IFilterTransactionService
    {
        Task<IList<ITransaction>> GetFilterTransactions(IFilterTransactionRequest request);
    }
}
