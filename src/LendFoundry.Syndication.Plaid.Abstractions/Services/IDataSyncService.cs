﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IDataSyncService
    {
        Task<bool> SyncInstitutionsAsync();

        Task<bool> SyncInstitutionsByIdAsync(string institutionId);
    }
}
