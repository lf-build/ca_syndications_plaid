﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IInstitutionService
    {
        Task<IList<InstitutionsFeatureVM>> GetFeatureInstitutionsAsync();

        Task<IList<InstitutionsVM>> InstitutionsGetAsync(int count, int offset);

        Task<IList<InstitutionsSearchVM>> InstitutionsSearchAsync(string search);

        Task<IList<InstitutionsSearchVM>> InstitutionsSearchListAsync();

        Task<IInstitutionsVM> InstitutionsDetailsAsync(string institutionId);
    }
}
