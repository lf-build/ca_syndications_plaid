﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// ICreateItemService
    /// </summary>
    public interface IItemService
    {
        /// <summary>
        /// CreateItemProcessAsync
        /// </summary>
        /// <param name="client">client</param>
        /// <param name="requestData">requestData</param>
        /// <returns></returns>
        Task<IResponseItemCreate> CreateItemAsync(IRequestItemCreate requestData);

        Task<IList<PlaidResponseItemDelete>> ItemDeleteAsync(IRequestItemDelete requestParam);
        Task<bool> ItemAccountDeleteAsync(IRequestItemDelete requestParam);
        Task<IResponseItemCreate> ItemCreateMFAAsync(IMFADocittRequest requestData);
        Task<IList<AccountsVM>> GetItemsWithAccounts(string applicantId);
        Task<IAccountSyncVM> GetAccountAsync(IRequestItemAccount request);
        Task UpdateAccountBalancesAsync(string webhookCode, string itemId, string newTransactions);
        Task<List<string>> DelinkItems(int days);
        Task<List<string>> DeleteToken();
        Task<IResponseItemCreate> ItemUpdateAsync(IRequestItemUpdate request);
        Task ProcessEvent(object data, EventInfo eventInfo);
        Task PlaidLinkClientData(object data, EventInfo eventInfo);
    }
}
