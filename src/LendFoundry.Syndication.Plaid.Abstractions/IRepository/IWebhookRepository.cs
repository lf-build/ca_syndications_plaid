﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IWebhookRepository :  IRepository<IWebhookLog>
    {
        bool AddWebhookLog(IWebhookLog webhookData);
    }
}
