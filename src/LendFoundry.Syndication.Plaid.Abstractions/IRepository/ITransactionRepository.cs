﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// ITransactionRepository
    /// </summary>
    public interface ITransactionRepository : IRepository<ITransaction>
    {
        /// <summary>
        /// GetAllTransactions
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>Transaction Collection</returns>
        IEnumerable<ITransaction> GetAllTransactions(string accountId);

        /// <summary>
        /// GetTransactionsByDateRange
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="startDate">startDate</param>
        /// <param name="endDate">endDate</param>
        /// <returns>Transaction Collection</returns>
        IList<ITransaction> GetTransactions(IRequestTransaction transactionRequest);

        /// <summary>
        /// GetTransactionStartEndDates
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>Transaction dates array</returns>
        List<DateTime> GetTransactionStartEndDates(string accountId);       

        /// <summary>
        /// AddTransaction
        /// </summary>
        /// <param name="transaction">transaction</param>
        /// <returns>string</returns>
        string AddTransaction(ITransaction transaction);

        /// <summary>
        /// AddTransactions
        /// </summary>
        /// <param name="transactions">transactions</param>
        /// <returns>string</returns>
        string AddTransactions(IEnumerable<ITransaction> transactions);

        /// <summary>
        /// UpdateTransaction
        /// </summary>
        /// <param name="transaction">transaction</param>
        /// <returns>string</returns>
        string UpdateTransaction(ITransaction transaction);

        /// <summary>
        /// RemoveTransactionByAccount
        /// </summary>
        /// <param name="plaidAccountId">plaidAccountId</param>
        /// <returns>bool</returns>
        bool RemoveTransactionByAccount(List<string> plaidAccountId);

        /// <summary>
        /// GetFilterTransactionData
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="startDate">startDate</param>
        /// <param name="endDate">endDate</param>
        /// <param name="transactionType">transactionType</param>
        /// <param name="transactionAmount">transactionAmount</param>
        /// <returns>list of transactions</returns>
        IList<ITransaction> GetFilterTransactionData(IFilterTransactionRequest request);
    }
}