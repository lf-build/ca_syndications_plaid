﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// IItemRepository
    /// </summary>
    public interface IItemRepository : IRepository<IItem>
    {
        /// <summary>
        /// GetItemByItemId
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns>Item Collection</returns>
        IList<IItem> GetItemsByApplicantId(string applicantId);

        /// <summary>
        /// GetAccountsInfoByItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>accounts</returns>
        IList<Accounts> GetAccountsInfoByItemId(string itemId);

        /// <summary>
        /// GetApplicantAllAccessToken
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns>All access token</returns>
        IList<string> GetApplicantAllAccessToken(string applicantId);

        /// <summary>
        /// GetAccountsByItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>All accounts</returns>
        IEnumerable<string> GetAccountsByItemId(string itemId);

        /// <summary>
        /// GetAccountsByApplicantId
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns>All accounts</returns>
        IList<string> GetAccountsByApplicantId(string applicantId);

        /// <summary>
        /// GetAccessTokenByItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>access token</returns>
        string GetAccessTokenByItemId(string itemId);

        /// <summary>
        /// GetItemIdByAccessToken
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <returns>Item id</returns>
        string GetItemIdByAccessToken(string accessToken);

        /// <summary>
        /// GetItemStatus
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="institutionId">institutionId</param>
        /// <returns>-1 if item not exists, 1 if item and account exists, 0 if item exist but all accounts unlinked</returns>
        ItemStatus GetItemStatus(string applicantId, string institutionId);

        /// <summary>
        /// GetItemByItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>return Item</returns>
        IItem GetItemByGivenItemId(string item_Id);

        /// <summary>
        /// GetItemGivenApplicantIdInstitutionId
        /// </summary>
        /// <param name="applicant_Id">applicant_Id</param>
        /// <param name="institution_Id">institution_Id</param>
        /// <returns></returns>
        IItem GetItemGivenApplicantIdInstitutionId(string applicant_Id, string institution_Id);

        /// <summary>
        /// GetItemByAccountId
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>return Item</returns>
        IItem GetItemByAccountId(string accountId);

        /// <summary>
        /// AddItem
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>string</returns>
        string AddItem(IItem item);

        /// <summary>
        /// UpdateItem
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>string</returns>
        void UpdateItem(IItem item);

        /// <summary>
        /// RemoveItem
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>true or false</returns>
        bool DeleteItemGivenItemId(string item_Id);

        /// <summary>
        /// RemoveItemByAccessToken
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <returns>true or false</returns>
        bool DeleteItemGivenAccessToken(string accessToken);

        /// <summary>
        /// RemoveAccountByAccountId
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="accountId">accountId</param>
        /// <returns>true or false</returns>
        bool RemoveAccountByAccountId(string applicantId, string accountId);

        /// <summary>
        /// UpdateAccountByAccountId
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="accountId">accountId</param>
        /// <param name="balance">balance</param>
        /// <returns>true or false</returns>
        bool UpdateAccountBalancesGivenAccountId(string applicantId, string accountId, Balances balance);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="noOfDaysBefore"></param>
        /// <param name="entityIds"></param>
        /// <returns></returns>
        IList<string> GetAccessToken(int noOfDaysBefore, List<string> entityIds);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="noOfDaysBefore"></param>
        /// <returns></returns>
        IList<string> GetAccessToken(int noOfDaysBefore);
        IList<FilterApplications> GetAccessTokenEntityId();
        /// <summary>
        /// UpdateAllItemsBeforeThreeMonths
        /// </summary>
        /// <param name="noOfDaysBefore"></param>
        /// <returns>true or false</returns>
        void UpdateAccessToNull(List<string> accessTokens);       
    }
}