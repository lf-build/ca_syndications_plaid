﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IFeatureInstitutionsRepository : IRepository<IFeatureInstitutions>
    {
        Task<List<IFeatureInstitutions>> GetFeatureInstitutionsAsync();
    }
}