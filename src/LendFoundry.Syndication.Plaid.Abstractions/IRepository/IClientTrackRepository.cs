﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    /// <summary>
    /// IClientTrackRepository
    /// </summary>
    public interface IClientTrackRepository : IRepository<IClientTrack>
    {
        
        /// <summary>
        /// AddItem
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>string</returns>
        string AddItem(IClientTrack item);
    }
}