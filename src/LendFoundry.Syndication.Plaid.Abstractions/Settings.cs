﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public static class Settings
    {
        public static string ServiceName { get; } = "plaid";

        private static string Prefix { get; } = ServiceName.ToUpper();

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

       public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "plaid");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static string Nats { get; } = Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";
        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISION_ENGINE", "decision-engine");
        public static ServiceSettings Cashflow { get; } = new ServiceSettings($"{Prefix}_CASHFLOW_HOST", "cashflow", $"{Prefix}_CASHFLOW_PORT");
        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUPSERVICE", "lookupservice");
        public static ServiceSettings TlsProxy { get; } = new ServiceSettings($"{Prefix}_TLSPROXY", "tlsproxy");


    }
}
