﻿namespace LendFoundry.Syndication.Plaid
{
    public interface IRequestItemDelete
    {
        string ApplicantId { get; set; }

        string PlaidAccountId { get; set; }
    }
}