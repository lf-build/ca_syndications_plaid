﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{
    public interface IPlaidCredential
    {
        string client_id { get; set; }

        string secret { get; set; }

        int count { get; set; }

        int offset { get; set; }
    }
}


