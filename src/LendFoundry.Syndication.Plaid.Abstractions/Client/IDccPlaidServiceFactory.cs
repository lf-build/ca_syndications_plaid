﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IDccPlaidServiceFactory
    {
        IDccPlaidService Create(ITokenReader reader);
    }
}
