﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IDccPlaidService
    {
        Task<string[]> DelinkItems(int noOfDays);

        Task<List<Transaction>> GetFilterTransactions(
            DateTime startDate,
            DateTime endDate,
            IList<string> accountId,
            string applicantId,
            double threshold,
            List<string> categoryIds);

        Task<List<IFeatureInstitutions>> GetFeatureInstitutions();

        Task<IList<AccountsVM>> GetItemsWithAccounts(string applicantId);
        Task<string[]> DeleteToken();
    }
}
