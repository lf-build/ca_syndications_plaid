﻿namespace LendFoundry.Syndication.Plaid
{
    public interface IPlaidCreatePublicTokenResponse
    {
        string PublicToken { get; set; }
        string RequestId { get; set; }
    }
}