﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{
    public interface IMFADocittRequest
    {
        string PublicToken { get; set; }

        string MfaType { get; set; }

        string[] Responses { get; set; }

        string ApplicantId { get; set; }

        string InstitutionId { get; set; }
    }

    public class MFADocittRequest : IMFADocittRequest
    {
        [JsonProperty("public_token")]
        public string PublicToken { get; set; }

        [JsonProperty("mfa_type")]
        public string MfaType { get; set; }

        [JsonProperty("responses")]
        public string[] Responses { get; set; }

        [JsonProperty("applicant_id")]
        public string ApplicantId { get; set; }

        [JsonProperty("institution_id")]
        public string InstitutionId { get; set; }
    }
}
