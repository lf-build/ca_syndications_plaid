﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{
    public class AccountsRequest : Aggregate, IAccountsRequest
    {
        public AccountsRequest()
        {
        }

        public AccountsRequest(IAccountsRequest request)
        {
            AccessToken = request.AccessToken;
            BankSupportedProductType = request.BankSupportedProductType;
            SelectedAccountId = request.SelectedAccountId;
        }

        public string AccessToken { get; set; }
        public string BankSupportedProductType { get; set; }
        public string SelectedAccountId { get; set; }
    }
}
