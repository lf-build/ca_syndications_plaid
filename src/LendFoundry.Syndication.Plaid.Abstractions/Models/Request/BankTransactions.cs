﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using CapitalAlliance.Cashflow;

namespace LendFoundry.Syndication.Plaid
{
    public class BankTransactions : IBankTransactions
    {
        public BankTransactions() { }

		// TODO compilation error : no implicit reference conversion from
		// TODO 'CapitalAlliance.Cashflow.Transaction' to
		// TODO 'CapitalAlliance.Cashflow.ITransaction'. Needs resolution
		// TODO I could not find any way to solve this hence adding TODO
		// TODO -- alok
        [JsonConverter(typeof(InterfaceListConverter<CapitalAlliance.Cashflow.ITransaction, CapitalAlliance.Cashflow.Transaction>))]
        public List<CapitalAlliance.Cashflow.ITransaction> Transactions { get; set; }

    }
}
