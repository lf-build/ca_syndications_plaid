﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Plaid
{
    public class PublicTokenExchangeRequest
    {
        [JsonProperty("client_id")]
        public string Client_Id { get; set; }

        [JsonProperty("secret")]
        public string Secret { get; set; }

        [JsonProperty("public_token")]
        public string Public_Token { get; set; }
    }
}
