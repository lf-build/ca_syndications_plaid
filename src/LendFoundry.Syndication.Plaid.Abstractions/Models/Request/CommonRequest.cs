﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Plaid
{
    public interface ICommonRequest
    {
        string Client_Id { get; set; }

        string Secret { get; set; }

        string Access_Token { get; set; }
    }

    public class CommonRequest : ICommonRequest
    {
        [JsonProperty("client_id")]
        public string Client_Id { get; set; }

        [JsonProperty("secret")]
        public string Secret { get; set; }

        [JsonProperty("access_token")]
        public string Access_Token { get; set; }
    }
}