﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Plaid
{
    public interface IFilterTransactionRequest
    {
        IList<string> AccountId { get; set; }
        string ApplicantId { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        string TransactionType { get; set; } // CR or DR
        double TransactionAmount { get; set; }
        IList<string> PlaidCategoryIds { get; set; }
        string Comparer { get; set; }
    }

    public class FilterTransactionRequest : IFilterTransactionRequest
    {
        [JsonProperty("account_id")]
        public IList<string> AccountId { get; set; }

        [JsonProperty("applicant_id")]
        public string ApplicantId { get; set; }

        [JsonProperty("start_date")]
        public DateTime StartDate { get; set; }

        [JsonProperty("end_date")]
        public DateTime EndDate { get; set; }

        [JsonProperty("transaction_type")]
        public string TransactionType { get; set; }

        [JsonProperty("transaction_amount")]
        public double TransactionAmount { get; set; }

        [JsonProperty("categoryIds")]
        public IList<string> PlaidCategoryIds { get; set; }

        [JsonProperty("comparer")]
        public string Comparer { get; set; }
    }
}
