﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Plaid
{
    public class RequestItemAccount : IRequestItemAccount
    {
        [JsonProperty("applicantId")]
        public string ApplicantId { get; set; }

        [JsonProperty("accountId")]
        public string PlaidAccountId { get; set; }
    }
}
