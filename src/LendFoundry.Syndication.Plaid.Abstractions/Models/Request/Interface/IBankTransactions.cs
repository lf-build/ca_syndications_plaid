﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Plaid
{
    public interface IBankTransactions
    {
        List<CapitalAlliance.Cashflow.ITransaction> Transactions { get; set; }
    }
}
