﻿namespace LendFoundry.Syndication.Plaid
{
    public interface IAccountsRequest
    {
        string AccessToken { get; set; }
        string BankSupportedProductType { get; set; }
        string SelectedAccountId { get; set; }
    }
}
