﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{
    public class RequestItemDelete : IRequestItemDelete
    {
        [JsonProperty("applicant_id")]
        public string ApplicantId { get; set; }

        [JsonProperty("plaid_account_id")]
        public string PlaidAccountId { get; set; }
    }
}
