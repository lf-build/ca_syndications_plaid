﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{
    public interface IInstitutionRequest
    {
        string client_id { get; set; }
        string secret { get; set; }
        int count { get; set; }
        int offset { get; set; }
    }

    public class InstitutionRequest : IInstitutionRequest
    {
        public string client_id { get; set; }
        public string secret { get; set; }
        public int count { get; set; }
        public int offset { get; set; }
    }
}
