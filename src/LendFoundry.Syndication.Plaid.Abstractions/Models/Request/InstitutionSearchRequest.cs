﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{

    public interface IInstitutionSearchRequest
    {
        string query { get; set; }

        string[] products { get; set; }

        string public_key { get; set; }
    }
    public class InstitutionSearchRequest : IInstitutionSearchRequest
    {
        public string query { get; set; }

        public string[] products { get; set; }

        public string public_key { get; set; }

        public SearchOptions options { get; set; }
    }

    public interface IInstitutionSearchByIdRequest
    {
        string institution_id { get; set; }
        string public_key { get; set; }
        SearchOptions options { get; set; }        
    }

    public class InstitutionSearchByIdRequest : IInstitutionSearchByIdRequest
    {
        public string institution_id { get; set; }
        public string public_key { get; set; }
        public SearchOptions options { get; set; }
    }
}
