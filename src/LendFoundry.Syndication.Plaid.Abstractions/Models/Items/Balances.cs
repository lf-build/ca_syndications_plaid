﻿using LendFoundry.Syndication.Plaid.Abstractions;

namespace LendFoundry.Syndication.Plaid
{
    public class Balances: IBalances
    {
        public Balances()
        {

        }
        public Balances(IBalances balances)
        {
            if (balances != null)
            {
                Current = balances.Current;
                Available = balances.Available;
                Limit = balances.Limit;
            }
        }
        public double? Available { get; set; }
        public double? Current { get; set; }
        public double? Limit { get; set; }
    }
}

