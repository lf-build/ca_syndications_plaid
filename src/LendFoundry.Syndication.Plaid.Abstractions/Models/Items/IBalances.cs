﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IBalances
    {
        double? Available { get; set; }
        double? Current { get; set; }
        double? Limit { get; set; }
    }
}
