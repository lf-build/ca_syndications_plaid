﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IPlaidDelink : IAggregate
    {
        string EntityId { get; set; }
        string AccessToken { get; set; }

        bool Deleted { get; set; }

        IPlaidResponseError error { get; set; }

        DateTime CreatedDate { get; set; }
    }
}
