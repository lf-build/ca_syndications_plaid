﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public class PlaidDelink : Aggregate, IPlaidDelink
    {
        public string EntityId { get; set; }

        public string AccessToken { get; set; }        

        public bool Deleted { get; set; }

        public IPlaidResponseError error { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
