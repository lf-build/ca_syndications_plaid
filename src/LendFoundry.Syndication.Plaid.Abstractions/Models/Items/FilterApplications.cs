﻿using LendFoundry.Foundation.Date;
using LendFoundry.Syndication.Plaid.Abstractions;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Plaid
{


    public class FilterApplications : IFilterApplications
    {
        public FilterApplications()
        {

        }
        public FilterApplications(IFilterApplicationsResult request)
        {
            foreach (var data in request.filterApplicationsResult)
            {
                AccessToken = data.AccessToken;
                ApplicationSubmitedDate = data.ApplicationSubmitedDate;
                EntiyId = data.EntiyId;
                ItemCreateOn = data.ItemCreateOn;
                Status = data.Status;
                StatusCode = data.StatusCode;
                StatusDate = data.StatusDate;
            }
        }
        public string EntiyId { get; set; }
        public string EntityType { get; set; }
        public TimeBucket ItemCreateOn { get; set; }
        public string StatusCode { get; set; }
        public string Status { get; set; }
        public TimeBucket StatusDate { get; set; }
        public TimeBucket ApplicationSubmitedDate { get; set; }
        public string AccessToken { get; set; }
    }
}

