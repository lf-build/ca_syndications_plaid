﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IFilterApplications
    {
        string EntiyId { get; set; }
        string EntityType { get; set; }       
        TimeBucket ItemCreateOn{ get; set; }
        string StatusCode { get; set; }
        string Status { get; set; }
        TimeBucket StatusDate { get; set; }
        TimeBucket ApplicationSubmitedDate { get; set; }
        string AccessToken { get; set; }
    }
}