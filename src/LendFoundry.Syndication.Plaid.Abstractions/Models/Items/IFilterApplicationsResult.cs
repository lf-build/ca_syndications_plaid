﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IFilterApplicationsResult
    {
        List<IFilterApplications> filterApplicationsResult { get; set; }
    }
}