﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid.Abstractions
{
    public interface IAccounts
    {

        string PlaidAccountId { get; set; }


        string AccountName { get; set; }


        string AccountType { get; set; }


        string Mask { get; set; }


        string OfficialName { get; set; }


        string SubType { get; set; }

        IBalances Balances { get; set; }

        bool IsLinked { get; set; }


        string OwnerName { get; set; }
    }
}
