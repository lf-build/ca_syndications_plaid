﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Plaid
{
    public class Item : Aggregate, IItem
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }       
        public string ItemId { get; set; }
        public string InstitutionId { get; set; }
        public string PublicToken { get; set; }
        public string AccessToken { get; set; }
        public List<Accounts> Accounts { get; set; }
        public TimeBucket CreatedDateTime { get; set; }
        public Identity Identity { get; set; }
    }    
}