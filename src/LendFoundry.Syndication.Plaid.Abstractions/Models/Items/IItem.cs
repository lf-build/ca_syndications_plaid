﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Plaid
{
    public interface IItem : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string ItemId { get; set; }
        string InstitutionId { get; set; }
        string PublicToken { get; set; }
        string AccessToken { get; set; }
        List<Accounts> Accounts { get; set; }
        TimeBucket CreatedDateTime { get; set; }
        Identity Identity { get; set; }
    }    
}