﻿using LendFoundry.Foundation.Date;
using LendFoundry.Syndication.Plaid.Abstractions;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Plaid
{


    public class FilterApplicationList : IFilterApplicationList
    {
        public FilterApplicationList()
        {

        }
        public IList<string> ApplicationNumber { get; set; }
    }
}

