﻿using LendFoundry.Syndication.Plaid.Abstractions;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Plaid
{
   

    public class Accounts : IAccounts
    {

        public Accounts()
        {

        }

        public Accounts(IAccounts accounts)
        {
            if (accounts != null)
            {
                PlaidAccountId = accounts.PlaidAccountId;
                AccountName = accounts.AccountName;
                Balances = new Balances(accounts.Balances);
                IsLinked = accounts.IsLinked;
                Mask = accounts.Mask;
                AccountType = accounts.AccountType;
                SubType = accounts.SubType;
                OfficialName = accounts.OfficialName;
                OwnerName = accounts.OwnerName;
                
            }
        }

        [JsonProperty("account_id")]
        public string PlaidAccountId { get; set; }

        [JsonProperty("name")]
        public string AccountName { get; set; }

        [JsonProperty("type")]
        public string AccountType { get; set; }

        [JsonProperty("mask")]
        public string Mask { get; set; }

        [JsonProperty("official_name")]
        public string OfficialName { get; set; }

        [JsonProperty("subtype")]
        public string SubType { get; set; }
        [JsonConverter(typeof(ConcreteJsonConverter<Balances>))]
        [JsonProperty("balances")]
        public IBalances Balances { get; set; }

        public bool IsLinked { get; set; } = true;

        [JsonProperty("ownerName")]
        public string OwnerName { get; set; }
    }
}

