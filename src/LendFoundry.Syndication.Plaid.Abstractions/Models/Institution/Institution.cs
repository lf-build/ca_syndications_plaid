﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Plaid
{
    public class Institutions : Aggregate, IInstitutions
    {
        private Institutions institutions;
        public Institutions()
        {

        }
        public Institutions(Institutions institutions)
        {
            Colors = institutions?.Colors;
            Credentials = institutions?.Credentials;
            InstitutionLogo = institutions?.InstitutionLogo;
            InstitutionName = institutions?.InstitutionName;
            PlaidInstitutionId = institutions?.PlaidInstitutionId;
            Products = institutions?.Products;
            UrlAccountLocked = institutions?.UrlAccountLocked;
            UrlAccountSetup = institutions?.UrlAccountSetup;
            UrlForgottenPassword = institutions?.UrlForgottenPassword;
        }

        [JsonProperty("institution_id")]
        public string PlaidInstitutionId { get; set; }

        [JsonProperty("name")]
        public string InstitutionName { get; set; }

        [JsonProperty("logo")]
        public string InstitutionLogo { get; set; }

        [JsonProperty("credentials")]
        public List<Credential> Credentials { get; set; }

        [JsonProperty("has_mfa")]
        public bool HasMFA { get; set; }

        [JsonProperty("products")]
        public string[] Products { get; set; }

        [JsonProperty("url_account_locked")]
        public string UrlAccountLocked { get; set; }

        [JsonProperty("url_account_setup")]
        public string UrlAccountSetup { get; set; }

        [JsonProperty("url_forgotten_password")]
        public string UrlForgottenPassword { get; set; }

        [JsonProperty("colors")]
        public Colors Colors { get; set; }
         
        public DateTime CreatedDateTime { get; set; }
    }
}