﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Syndication.Plaid.Abstractions;
using Newtonsoft.Json;
using System;

namespace LendFoundry.Syndication.Plaid
{
    public class Transaction : Aggregate, ITransaction
    {
        public Transaction()
        {

        }

        public Transaction(ITransaction transaction)
        {
            if (transaction != null)
            {
                PlaidAccountId = transaction.PlaidAccountId;
                AccountOwner = transaction.AccountOwner;
                Amount = transaction.Amount;
                Category = transaction.Category;
                CategoryId = transaction.CategoryId;
                Date = transaction.Date;
                //Description = transaction.Description;
                Location = transaction.Location;
                Name = transaction.Name;
                Pending = transaction.Pending;
                PendingTransactionId = transaction.PendingTransactionId;
                PlaidTransactionId = transaction.PlaidTransactionId;
                Type = transaction.Type;
            }
        }

        // public string AccountId { get; set; }

        [JsonProperty("account_id")]
        public string PlaidAccountId { get; set; }

        [JsonProperty("transaction_id")]
        public string PlaidTransactionId { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("category")]
        public string[] Category { get; set; }

        [JsonProperty("category_id")]
        public string CategoryId { get; set; }

        [JsonProperty("transaction_type")]
        public string Type { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("pending")]
        public bool Pending { get; set; }


        [JsonProperty("pending_transaction_id")]
        public string PendingTransactionId { get; set; }

        //[JsonProperty("name")]
        //public string Description { get; set; }

        [JsonProperty("account_owner")]
        public string AccountOwner { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonConverter(typeof(ConcreteJsonConverter<Location>))]
        [JsonProperty("location")]
        public ILocation Location { get; set; }

    }
}