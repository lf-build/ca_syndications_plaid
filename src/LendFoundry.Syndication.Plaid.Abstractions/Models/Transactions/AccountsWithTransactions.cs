﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Syndication.Plaid
{
    public class AccountsWithTransactions: IAccountsWithTransactions
    {
        public string AccountId { get; set; }

        public Transaction Transactions { get; set; }
    }
}