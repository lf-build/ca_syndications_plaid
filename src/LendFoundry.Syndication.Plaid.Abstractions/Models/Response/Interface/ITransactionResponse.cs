﻿using LendFoundry.Syndication.Plaid.Abstractions;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Plaid
{
    public interface ITransactionResponse
    {
        IList<IAccounts> accounts { get; set; }
        ItemViewModel item { get; set; }
        string request_id { get; set; }
        int total_transactions { get; set; }
        List<ITransaction> transactions { get; set; }

        string AccessToken { get; set; }
        string PLAIDResponse { get; set; }
        string PLAIDError { get; set; }
        string ErrorMessage { get; set; }
        int ErrorCode { get; set; }
    }
}