﻿namespace LendFoundry.Syndication.Plaid
{
    public interface ILocation
    {
       string State { get; set; }
       string City { get; set; }
       string Address { get; set; }
       string StoreNumber { get; set; }
       string Zip { get; set; }
       string Lat { get; set; }
       string Lon { get; set; }
    }
}