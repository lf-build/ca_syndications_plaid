﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{
    public class DeviceListViewModel
    {
        public string device_id { get; set; }
        public string mask { get; set; }
        public string type { get; set; }
    }
}
