﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Plaid
{
    public class InstitutionsResponse : IInstitutionsResponse
    {
        public InstitutionsResponse()
        {

        }
        public IList<InstitutionsVM> institutions { get; set; }

        public int total { get; set; }
    }

    public class InstitutionsUpdateResponse : IInstitutionsUpdateResponse
    {
        public InstitutionsUpdateResponse()
        {

        }
        public Institutions institution { get; set; }
    }
}