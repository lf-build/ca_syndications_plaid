﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Plaid
{
    public interface IAccountsVM
    {
        string PlaidItemId { get; set; }

        string InstitutionId { get; set; }

        string Name { get; set; }

        string Logo { get; set; }

        List<Accounts> Accounts { get; set; }

         bool HasError { get; set; }

         PlaidResponseError Error { get; set; }
    }

    public class AccountsVM : IAccountsVM
    {
        [JsonProperty("plaid_item_id")]
        public string PlaidItemId { get; set; }

        [JsonProperty("institutionId")]
        public string InstitutionId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("accounts")]
        public List<Accounts>  Accounts{ get; set; }

        [JsonProperty("has_error")]
        public bool HasError { get; set; }

        [JsonProperty("error")]
        public PlaidResponseError Error { get; set; }
    }
}
