﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{
    public class Location : ILocation
    {
        public Location()
        {
        }

        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string StoreNumber { get; set; }
        public string Zip { get; set; }
        public string Lat { get; set; }
        public string Lon { get; set; }
    }
}
