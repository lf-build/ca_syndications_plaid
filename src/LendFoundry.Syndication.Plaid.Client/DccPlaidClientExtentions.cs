﻿using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Syndication.Plaid.Client
{
    public static class DccPlaidServiceExtentions
    {
        public static IServiceCollection AddDccPlaidService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IDccPlaidServiceFactory>(p => new DccPlaidServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IDccPlaidServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}