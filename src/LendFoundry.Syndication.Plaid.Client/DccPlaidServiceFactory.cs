﻿using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

namespace LendFoundry.Syndication.Plaid.Client
{ 
    public class DccPlaidServiceFactory : IDccPlaidServiceFactory
    {
        public DccPlaidServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        } 

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IDccPlaidService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new DccPlaidService(client);
        }
    }
}