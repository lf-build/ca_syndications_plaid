﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using System.Web;
#else
using RestSharp.Contrib;
#endif

namespace LendFoundry.Syndication.Plaid.Client
{
    public class DccPlaidService : IDccPlaidService
    {
        public DccPlaidService(IServiceClient client)
        {
            this.Client = client;
        }

        private IServiceClient Client { get; }

        /// <summary>
        /// Delink Item with Plaid
        /// </summary>
        /// <param name="noOfDays">lookup days</param>
        /// <returns></returns>
        public async Task<string[]> DelinkItems(int noOfDays)
        {
            var request = new RestRequest($"item/delink/{noOfDays}", Method.POST);
            return await Client.ExecuteAsync<string[]>(request);
        }
        public async Task<string[]> DeleteToken()
        {
            var request = new RestRequest("item/deletetoken", Method.POST);
            return await Client.ExecuteAsync<string[]>(request);
        }

        public async Task<List<Transaction>> GetFilterTransactions(
            DateTime startDate,
            DateTime endDate,
            IList<string> accountId, 
            string applicantId, 
            double threshold, 
            List<string> categoryIds)
        {
            var request = new RestRequest($"/filtertransaction", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();

            IFilterTransactionRequest requestData = new FilterTransactionRequest()
            {
                AccountId = accountId,
                ApplicantId = applicantId,
                StartDate = startDate,
                EndDate = endDate,
                TransactionType = "CR",
                TransactionAmount = threshold,
                PlaidCategoryIds = categoryIds,
                Comparer = ">"
            };

            request.AddJsonBody(requestData);
            return await Client.ExecuteAsync<List<Transaction>>(request);
        }

        #region Institutions

        /// <summary>
        /// Get the list of feature institutions
        /// </summary>
        /// <returns>list of institutions</returns>
        public async Task<List<IFeatureInstitutions>> GetFeatureInstitutions()
        {
            var request = new RestRequest("institutions/feature", Method.GET);
            return await Client.ExecuteAsync<List<IFeatureInstitutions>>(request);
        }

        /// <summary>
        /// Search Institutions
        /// </summary>
        /// <param name="searchkey">search key words</param>
        /// <returns>list of institutions</returns>
        public async Task<List<InstitutionsVM>> GetInstitutions(string searchkey)
        {
            string resource = string.Format($"institutions/search/{searchkey}");
            var request = new RestRequest(resource, Method.GET);
            return await Client.ExecuteAsync<List<InstitutionsVM>>(request);
        }

        /// <summary>
        /// Get the list of institions
        /// </summary>
        /// <param name="count">no of records</param>
        /// <param name="offset">starting positions</param>
        /// <returns>list of institutions</returns>
        public async Task<List<InstitutionsVM>> GetInstitutions(int count, int offset)
        {
            string resource = string.Format($"institutions/get/{count}/{offset}");
            var request = new RestRequest(resource, Method.GET);
            return await Client.ExecuteAsync<List<InstitutionsVM>>(request);
        }

        #endregion Institutions

        #region Item

        /// <summary>
        /// Get the list of feature institutions
        /// </summary>
        /// <returns>list of institutions</returns>
        public async Task<IList<AccountsVM>> GetItemsWithAccounts(string applicantId)
        {
            var encodedApplicantId = HttpUtility.UrlEncode(applicantId);
            var request = new RestRequest($"item/accounts/all/{encodedApplicantId}", Method.GET);
            return await Client.ExecuteAsync<IList<AccountsVM>>(request);
        }
               #endregion
    }
}
