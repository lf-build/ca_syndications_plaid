﻿using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Tenant.Client;
using System;
using System.Linq;
using System.Reflection;
using LendFoundry.Security.Tokens;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace LendFoundry.Syndication.Plaid
{
    public class PlaidListener : IPlaidListener
    {
        public PlaidListener
        (
            IConfigurationServiceFactory<Configuration> apiConfigurationFactory,
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IItemServiceFactory itemClientServiceFactory,
             ITenantTimeFactory tenantTimeFactory
        )
        {
            this.EventHubFactory = eventHubFactory;
            this.ApiConfigurationFactory = apiConfigurationFactory;
            this.TokenHandler = tokenHandler;
            this.TenantServiceFactory = tenantServiceFactory;
            this.Logger = loggerFactory.Create(NullLogContext.Instance);
            this.ItemClientServiceFactory = itemClientServiceFactory;
            this.TenantTimeFactory = tenantTimeFactory;
            this.ConfigurationFactory = configurationFactory;
            Logger.Debug($"Plaid Listner constructor");
        }

        private IConfigurationServiceFactory<Configuration> ApiConfigurationFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILogger Logger { get; }

        private IItemServiceFactory ItemClientServiceFactory { get; }

        public void Start()
        {
            Logger.Debug($"Eventhub listener started");
            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                Logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    Logger.Info($"Initializing event hub for teanant #{tenant.Id}");
                    // Tenant token creation         
                    var token = TokenHandler.Issue(tenant.Id, "PlaidIssuer");
                    var reader = new StaticTokenReader(token.Value);

                    // Needed resources for this operation
                    var hub = EventHubFactory.Create(reader);
                    var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                    //  var configuration = ApiConfigurationFactory.Create(reader).Get();
                    var configuration = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader).Get();
                    if (configuration == null)
                        throw new ArgumentException("Api configuration cannot be found, please check");

                    var itemService = ItemClientServiceFactory.Create(reader, TokenHandler, Logger);

                    // Attach all configured events to be listen
                    configuration
                    .events
                        .ToList()
                        .ForEach(eventConfig =>
                        {
                            Logger.Info("----------------------------------------");
                            Logger.Info($"New subscription attached to tenant #{tenant.Id} event #{eventConfig.Name}");
                            Logger.Info("----------------------------------------");
                            hub.On(eventConfig.Name, ProcessEvent(eventConfig,this.Logger, itemService));
                        });
                    hub.StartAsync();
                });
            }
            catch (Exception ex)
            {
                Logger.Error("Error while listening eventhub", ex);
                Start();
            }
        }

        #region Process Event
        private Action<LendFoundry.EventHub.EventInfo> ProcessEvent(
           EventConfiguration eventConfiguration,
           ILogger logger,
           IItemService itemService
           )
        {
            return async @event =>
            {
                string responseData = string.Empty;
                string responseXmlData = string.Empty;
                object data = null;
                try
                {
                    try
                    {
                        responseData = eventConfiguration.Response.FormatWith(@event); //posted data
                        responseXmlData = eventConfiguration.ResponseObject.FormatWith(@event); //posted xml data
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData);
                    }
                    catch { }
                    //var entityId = eventConfiguration.EntityId.FormatWith(@event.Data);
                   // var entityType = eventConfiguration.EntityType.FormatWith(@event);
                    var Name = eventConfiguration.Name.FormatWith(@event);
                    var methodName = eventConfiguration.MethodToExecute;
                    MethodInfo method = typeof(IItemService).GetMethod(methodName);
                    object[] param = {data, @event };
                    await (dynamic)method.Invoke(itemService, param);
                   // logger.Info($"Entity Id: #{entityId}");
                    logger.Info($"Response Data : #{data}");
                    logger.Info($"Plaid event {Name} completed.");

                }
                catch (Exception ex)
                {
                    logger.Error($"Unhandled exception while listening event {@event.Name}", ex);
                }
            };
        }
        #endregion
    }
}
