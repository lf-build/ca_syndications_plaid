﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CapitalAlliance.Cashflow;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Syndication.Plaid.Abstractions.Events;
using Newtonsoft.Json;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace LendFoundry.Syndication.Plaid
{
    /// <summary>
    /// CreateItemService
    /// </summary>
    public class ItemService : IItemService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemService"/> class.
        /// </summary>
        /// <param name="logger">logger</param>
        public ItemService(
            ILogger logger,
            IPlaidClient plaidClientService,
            ITenantTime tenantTime,
            IConfiguration configuration,
            IInstitutionsRepository institutionRepository,
            IItemRepository itemRepository,
            Abstractions.ITransactionRepository transactionRepository,
            IWebhookRepository webhookRepository,
            IPlaidDelinkRepository plaidDelinkRepository,
            ITokenReader tokenReader,
            ITokenHandler tokenParser,
            IEventHubClient eventHub,
            IDecisionEngineService decisionEngineService,
            ILookupService lookup,
            ICashflowService cashflowService,
            IClientTrackRepository clientTrackRepository
        )
        {
            if (tokenReader == null) throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            if (tokenParser == null) throw new ArgumentException($"{nameof(tokenParser)} is mandatory");
            if (configuration == null) throw new ArgumentException("Plaid configuration cannot be found, please check");
            if (configuration == null) throw new ArgumentException("Plaid configuration cannot be found, please check");
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            if (decisionEngineService == null)
                throw new ArgumentNullException(nameof(decisionEngineService));
            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            this.TenantTime = tenantTime;
            this.CommandExecutor = new CommandExecutor(logger);
            this.Logger = logger;
            this.PlaidClientService = plaidClientService;
            this.InstitutionRepository = institutionRepository;
            this.ItemRepository = itemRepository;
            this.TransactionRepository = transactionRepository;
            this.Configuration = configuration;
            this.PlaidDelinkRepository = plaidDelinkRepository;
            this.TokenReader = tokenReader;
            this.TokenParser = tokenParser;
            this.WebhookRepository = webhookRepository;
            this.EventHub = eventHub;
            this.DecisionEngineService = decisionEngineService;
            this.Lookup = lookup;
            this.CashflowService = cashflowService;
            this.ClientTrackRepository = clientTrackRepository;
        }
        /// <summary>
        /// EventHub
        /// </summary>
        private IEventHubClient EventHub { get; }
        /// <summary>
        /// PlaidDelinkRepository repository
        /// </summary>
        private IPlaidDelinkRepository PlaidDelinkRepository { get; }

        /// <summary>
        /// Get Tenant Time 
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Get TokenReader
        /// </summary>
        private ITokenReader TokenReader { get; }

        /// <summary>
        /// Token Parser
        /// </summary>
        private ITokenHandler TokenParser { get; }

        /// <summary>
        /// Gets PlaidClientService
        /// </summary>
        private IPlaidClient PlaidClientService { get; }

        /// <summary>
        /// Gets CommandExecutor
        /// </summary>
        private CommandExecutor CommandExecutor { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets ItemRepository
        /// </summary>
        private IItemRepository ItemRepository { get; }

        /// <summary>
        /// Gets ClientTrackRepository
        /// </summary>
        private IClientTrackRepository ClientTrackRepository { get; }

        /// <summary>
        /// Get Logger
        /// </summary>
        private ILogger Logger { get; }

        /// <summary>
        /// Gets TransactionRepository
        /// </summary>
        private Abstractions.ITransactionRepository TransactionRepository { get; }

        /// <summary>
        /// Get InstitutionRepository
        /// </summary>
        private IInstitutionsRepository InstitutionRepository { get; }

        /// <summary>
        /// Gets WebhookRepository
        /// </summary>
        private IWebhookRepository WebhookRepository { get; }
        /// <summary>
        /// Get DecisionEngineService
        /// </summary>
        private IDecisionEngineService DecisionEngineService { get; }
        /// <summary>
        /// lookup
        /// </summary>
        private ILookupService Lookup { get; }
        /// <summary>
        /// Cashflow
        /// </summary>
        private ICashflowService CashflowService { get; }
        public async Task UpdateAccountBalancesAsync(string webhookCode, string itemId, string newTransactions)
        {
            this.Logger.Info("Currently it is triggered as : " + webhookCode);
            WebhookLog log = new WebhookLog()
            {
                ItemId = itemId,
                NewTransactions = newTransactions,
                WebhookCode = webhookCode,
                CreatedDate = TenantTime.Now
            };

            this.WebhookRepository.Add(log);

            if (webhookCode == "INITIAL_UPDATE" || webhookCode == "HISTORICAL_UPDATE")
            {
                //// update accounts and balances
                var itemResult = this.ItemRepository.GetItemByGivenItemId(itemId);
                this.Logger.Info("Get Access token : " + itemResult.AccessToken);
                //// Plaid call for the new account balance sync
                //// update only balances for the accounts
                var updatedAccounts = await this.PlaidClientService.GetAccountAsync(itemResult.AccessToken);
                foreach (var account in itemResult.Accounts)
                {
                    var updatedAcc = updatedAccounts.Accounts.FirstOrDefault(x => x.PlaidAccountId == account.PlaidAccountId);
                    account.Balances = updatedAcc.Balances;
                    break;
                }

                this.ItemRepository.UpdateItem(itemResult);
                this.Logger.Info("Item account balances are updated");
                //// Add new transactions
                var plaidTransactionRequest = this.TransactionRequestCreate(itemResult.AccessToken, newTransactions);
                var result = await this.PlaidClientService.PullTransactionAsync(plaidTransactionRequest);
                var transactionMessage = this.TransactionRepository.AddTransactions(result.transactions);
                this.Logger.Info("item transaction are added");
            }
            ////throw new NotImplementedException();
        }

        //// Use for creating plaid item , returns account information , request Id and Public token.
        /// <summary>
        /// CreateItemAsync
        /// </summary>
        /// <param name="client"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public async Task<IResponseItemCreate> CreateItemAsync(IRequestItemCreate requestData)
        {
            IResponseItemCreate response = new ResponseItemCreate();
            var itemStatus = this.ItemRepository.GetItemStatus(requestData.ApplicantId, requestData.InstitutionId);

            switch (itemStatus)
            {
                case ItemStatus.AccountNotExists:
                    var resultDelink = await this.Delinking(requestData);
                    if (resultDelink.Deleted == true)
                    {
                        response = await this.PlaidLinking(requestData);
                    }
                    break;
                case ItemStatus.NoRecordsFound:
                    response = await this.PlaidLinking(requestData);
                    break;

                case ItemStatus.AccountExists:
                    response.HasError = true;
                    response.Error = new PlaidResponseError
                    {
                        DisplayMessage = "Item is already linked.",
                        ErrorMessage = "already linked",
                        ErrorCode = string.Empty,
                        ErrorType = string.Empty
                    };
                    break;
            }

            return response;
        }

        /// <summary>
        /// PlaidLinking
        /// </summary>
        /// <param name="requestData">requestData</param>
        /// <returns>response for linked item</returns>
        private async Task<IResponseItemCreate> PlaidLinking(IRequestItemCreate requestData)
        {
            this.Logger.Info("Inside PlaidLinking... ");
            var response = await PlaidClientService.CreateItemExecutionFlow(requestData);

            // //// Save accounts info to mongo db and further process...
            return await this.GenericLinkedItem(response, requestData);
        }

        /// <summary>
        /// MFAServiceAsync
        /// </summary>
        /// <param name="client"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public async Task<IResponseItemCreate> ItemCreateMFAAsync(IMFADocittRequest requestData)
        {
            var response = await PlaidClientService.CreateMfaExecutionFlow(requestData);
            //// Save accounts info to mongo db and further process...
            return await this.GenericLinkedItem(response, requestData);
        }

        /// <summary>
        /// Delete an account within Item linked on plaid. 
        /// </summary>
        /// <returns>response</returns>
        public async Task<bool> ItemAccountDeleteAsync(IRequestItemDelete requestParam)
        {
            return await Task.Run(() =>
            {
                return this.ItemRepository.RemoveAccountByAccountId(requestParam.ApplicantId, requestParam.PlaidAccountId);
            });
        }

        /// <summary>
        /// Delete an Item linked on plaid. 
        /// </summary>
        /// <returns>response</returns>
        public async Task<IList<PlaidResponseItemDelete>> ItemDeleteAsync(IRequestItemDelete requestParam)
        {
            //// Get list of access tokens given applicant id
            var accessTokens = ItemRepository.GetApplicantAllAccessToken(requestParam.ApplicantId);

            //// Delink from Plaid
            var response = await PlaidClientService.ItemDeleteAsync(accessTokens);

            //// Delete from  Database 
            foreach (var obj in response.Where(x => x.Deleted == true))
            {
                foreach (var at in accessTokens)
                {
                    if (obj.AccessToken == at.Substring(15, 8))
                    {
                        var item_id = ItemRepository.GetItemIdByAccessToken(at);
                        var accounts = ItemRepository.GetAccountsByItemId(item_id);
                        // Delete transaction information 
                        this.TransactionRepository.RemoveTransactionByAccount(accounts.ToList());

                        this.ItemRepository.DeleteItemGivenAccessToken(at);
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// GetAccounts
        /// </summary>
        /// <returns>response</returns>
        public async Task<IList<AccountsVM>> GetItemsWithAccounts(string applicantId)
        {
            var itemResult = this.ItemRepository.GetItemsByApplicantId(applicantId);
            return await this.MapperViewModel(itemResult);
        }

        /// <summary>
        /// GetAccountAsync
        /// </summary>
        /// <returns>response</returns>
        public async Task<IAccountSyncVM> GetAccountAsync(IRequestItemAccount request)
        {
            IAccountSyncVM result = new AccountSyncVM();

            ////  Get Item detais from the ApplicantId and InstitutionId  as well as access token
            var itemData = this.ItemRepository.GetItemByAccountId(request.PlaidAccountId);

            if (itemData == null)
            {
                throw new InvalidArgumentException("Account is not exists");
            }
            else
            {
                //// Plaid call for the new account balance sync
                var updatedAccountsFromPlaid = await this.PlaidClientService.GetAccountAsync(itemData.AccessToken);

                //// update only balances for the accounts
                //if (!updatedAccountsFromPlaid.HasError)
                //{
                //    var updatedAccount = updatedAccountsFromPlaid.Accounts.FirstOrDefault(x => x.PlaidAccountId == request.PlaidAccountId);
                //    var itemResult = itemData.Accounts.FirstOrDefault(x => x.PlaidAccountId == request.PlaidAccountId);
                //    itemResult.Balances = updatedAccount.Balances;
                //    result.Account = itemResult;
                //    var isUpdated = this.ItemRepository.UpdateAccountBalancesGivenAccountId(request.ApplicantId, request.PlaidAccountId, updatedAccount.Balances);
                //}

                result.PlaidItemId = itemData.ItemId;
                result.Error = updatedAccountsFromPlaid.Error;
                result.HasError = updatedAccountsFromPlaid.HasError;
            }
            return await Task.Run(() =>
            {
                return result;
            });
        }

        /// <summary>
        /// ItemUpdateAsync
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>Response Item Create</returns>
        public async Task<IResponseItemCreate> ItemUpdateAsync(IRequestItemUpdate request)
        {
            //// Get accesstoken from the Plaid Item Id
            var resultItem = this.ItemRepository.GetItemByGivenItemId(request.PlaidItemId);

            if (resultItem == null || string.IsNullOrWhiteSpace(resultItem.AccessToken))
            {
                throw new InvalidArgumentException("Item is not exist");
            }

            //// Creating public token new for the item
            var publicToken = await this.PlaidClientService.CreatingPublicTokenAsync(resultItem.AccessToken);

            if (string.IsNullOrWhiteSpace(publicToken))
            {
                throw new InvalidArgumentException("public token is not generated for the item , please try again");
            }

            //// update public token in DB - For the data correct.
            resultItem.PublicToken = publicToken;
            this.ItemRepository.UpdateItem(resultItem);

            /// return as it response from plaid , no updates in DB
            var response = await this.PlaidClientService.UpdateItemExecutionFlow(request, publicToken);

            return await this.GenericLinkedItem(response, request);
        }

        private async Task<IPlaidResponseItemDelete> Delinking(IRequestItemCreate requestData)
        {
            var itemData = this.ItemRepository.GetItemGivenApplicantIdInstitutionId(requestData.ApplicantId, requestData.InstitutionId);
            IPlaidResponseItemDelete result = new PlaidResponseItemDelete();
            //// If item result not exists
            if (itemData == null)
            {
                result.Deleted = true;
                return result;
            }

            //// Remove linking from Plaid
            var responseList = await this.PlaidClientService.ItemDeleteAsync(new List<string>() { itemData.AccessToken });
            result = responseList.FirstOrDefault(); //// TODO - change once time
            if (result.Deleted == true)
            {
                //// Remove transactions Details 
                this.TransactionRepository.RemoveTransactionByAccount(itemData.Accounts.Select(x => x.PlaidAccountId).ToList<string>());
                //// Remove Item and Accounts Details
                this.ItemRepository.DeleteItemGivenItemId(itemData.ItemId);
            }
            return result;
        }
        /// <summary>
        /// DeleteToken
        /// </summary>       
        /// <returns></returns>
        public async Task<List<string>> DeleteToken()
        {

            var delinkedAccessToken = new List<string>();
            var result = new List<string>();
            Logger.Info("started Get Access Token's EntityId");
            var entityIds = new FilterApplicationList();
            var accessTokenEntityIds = this.ItemRepository.GetAccessTokenEntityId();
            if (accessTokenEntityIds == null && accessTokenEntityIds.Count <= 0)
                throw new ArgumentNullException($"{nameof(accessTokenEntityIds)} no access token found from items to perform delete token");
            Logger.Info("ended GetAccessTokenEntityId total:" + accessTokenEntityIds.Count + " found access token.");
            entityIds.ApplicationNumber = accessTokenEntityIds.Select(x => x.EntiyId).ToList();
            Logger.Info("started ApplicationFilterService Get GetApplications Details By Entity Ids");

            Logger.Info("started" + Configuration.DeleteTokenRule + " rule execution");
            var ruleResult = DecisionEngineService.Execute<dynamic, dynamic>(Configuration.DeleteTokenRule, new { input = entityIds });
            Logger.Info("Ended" + Configuration.DeleteTokenRule + " rule execution");
            IFilterApplicationsResult FilterobjAccounts = ExecuteRequest<FilterApplicationsResult>(ruleResult + "");
            if (FilterobjAccounts == null && FilterobjAccounts.filterApplicationsResult.Count <= 0)
                throw new ArgumentNullException($"no access token EntityId found from Application Filter to perform delete token");
            Logger.Info("ended Get Applications Details By Entity Ids total:" + FilterobjAccounts.filterApplicationsResult.Count() + " found from application filter to perform delete access token.");
            var objAccessTokenList = from itemdata in accessTokenEntityIds
                                     join filterappdata in FilterobjAccounts.filterApplicationsResult on itemdata.EntiyId equals filterappdata.EntiyId
                                     select new FilterApplications()
                                     {
                                         EntiyId = itemdata.EntiyId,
                                         AccessToken = itemdata.AccessToken,
                                         ItemCreateOn = itemdata.ItemCreateOn,
                                         ApplicationSubmitedDate = filterappdata.ApplicationSubmitedDate,
                                         Status = filterappdata.Status,
                                         StatusCode = filterappdata.StatusCode,
                                         StatusDate = filterappdata.StatusDate
                                     };
            IFilterApplicationsResult filterListApp = new FilterApplicationsResult();
            filterListApp.filterApplicationsResult = objAccessTokenList.ToList<IFilterApplications>();
            if (filterListApp == null && filterListApp.filterApplicationsResult.Count <= 0)
                throw new ArgumentNullException($"{nameof(filterListApp.filterApplicationsResult)}rule result: no application eligible to delete token");
            filterListApp.filterApplicationsResult.RemoveAll(x => x.AccessToken == null);
            if (filterListApp != null && filterListApp.filterApplicationsResult.Count > 0)
            {
                foreach (var accesToken in filterListApp.filterApplicationsResult)
                {
                    if (string.IsNullOrEmpty(accesToken.AccessToken))
                        continue;
                    //// delink item from Plaid
                    var response = await this.PlaidClientService.ItemDeleteNewAsync(accesToken.AccessToken);
                    delinkedAccessToken.Add(response.AccessToken);
                    IPlaidDelink data = new PlaidDelink()
                    {
                        EntityId = accesToken.EntiyId,
                        AccessToken = accesToken.AccessToken,
                        Deleted = response.Deleted,
                        error = response.error,
                        CreatedDate = DateTime.UtcNow
                    };
                    await EventHub.Publish("plaidDelinkPerformed", new
                    {
                        EntityId = accesToken.EntiyId,
                        Deleted = response.Deleted,
                        Response = response.error,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                    this.PlaidDelinkRepository.Add(data);
                    result.Add(data.Id);
                }
                // update database
                this.ItemRepository.UpdateAccessToNull(delinkedAccessToken);
            }
            Logger.Info("Ended delete token process..deleted token performed" + result.Count);
            return result;
        }

        /// <summary>
        /// Delink Items
        /// </summary>
        /// <returns>return true or false</returns>
        public async Task<List<string>> DelinkItems(int days)
        {
            var delinkedAccessToken = new List<string>();
            var result = new List<string>();

            var accessTokenList = this.ItemRepository.GetAccessToken(days);

            if (accessTokenList != null && accessTokenList.Count() > 0)
            {
                foreach (var accesToken in accessTokenList)
                {
                    if (string.IsNullOrEmpty(accesToken))
                        continue;

                    //// delink item from Plaid
                    var response = await this.PlaidClientService.ItemDeleteNewAsync(accesToken);

                    delinkedAccessToken.Add(response.AccessToken);

                    IPlaidDelink data = new PlaidDelink()
                    {
                        AccessToken = accesToken,
                        Deleted = response.Deleted,
                        error = response.error,
                        CreatedDate = DateTime.UtcNow
                    };

                    this.PlaidDelinkRepository.Add(data);
                    result.Add(data.Id);

                }

                // update database
                this.ItemRepository.UpdateAccessToNull(delinkedAccessToken);
            }
            return result;
        }

        /// <summary>
        /// MapperViewModel
        /// </summary>
        /// <param name="resultItem">resultItem</param>
        /// <returns>response</returns>
        private async Task<IList<AccountsVM>> MapperViewModel(IList<IItem> resultItem)
        {
            var plaidItems = new List<AccountsVM>();
            foreach (var singleItem in resultItem)
            {
                var data = new AccountsVM();
                data.PlaidItemId = singleItem.ItemId;
                data.InstitutionId = singleItem.InstitutionId;

                //// Get the name and logo
                var institutionDetails = await this.InstitutionRepository.GetInstitutionByIdAsync(data.InstitutionId);
                if (institutionDetails != null)
                {
                    data.Name = institutionDetails.InstitutionName;
                    data.Logo = institutionDetails.InstitutionLogo;
                }

                var accountList = new List<Accounts>();
                foreach (var singleAccount in singleItem.Accounts)
                {
                    accountList.Add(singleAccount);
                }
                data.Accounts = accountList;
                plaidItems.Add(data);
            }
            return plaidItems;
        }

        private IItem ItemMapper(IResponseItemCreate response, dynamic requestData, string accessToken, string itemId)
        {
            var item = new Item
            {
                Accounts = response.PlaidLink.Accounts,
                Identity = response.PlaidLink.Identity,
                EntityId = requestData.ApplicantId,
                AccessToken = accessToken,
                InstitutionId = requestData.InstitutionId,
                ItemId = itemId,
                PublicToken = response.PlaidLink.PublicToken,
                CreatedDateTime = new TimeBucket(DateTime.UtcNow)
            };

            return item;
        }

        /// <summary>
        /// TransactionRequestCreate
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <param name="newTransactions">newTransactions</param>
        /// <returns>return PlaidRequestTransaction</returns>
        private IPlaidRequestTransaction TransactionRequestCreate(string accessToken, string newTransactions)
        {
            return new PlaidRequestTransaction
            {
                AccessToken = accessToken,
                ClientId = this.Configuration.ClientId,
                Secret = this.Configuration.Secret,
                //Options = new Options1
                //{
                //    Count = Convert.ToInt32(newTransactions),
                //    Offset = 0
                //},
                StartDate = this.CreatingStartDate(),
                EndDate = DateTime.UtcNow.Date.ToString("yyyy-MM-dd"),

            };
        }

        /// <summary>
        /// CreatingStartDate
        /// </summary>
        /// <returns>return startdate</returns>
        private string CreatingStartDate()
        {
            return DateTime.UtcNow.AddYears(-2).Date.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response">response</param>
        /// <param name="requestData">requestData</param>
        /// <returns>Response</returns>
        private async Task<IResponseItemCreate> GenericLinkedItem(IResponseItemCreate response, dynamic requestData)
        {
            if (response.IsLinked == true && response.PlaidLink.PublicToken != null)
            {
                var accessTokenServiceResponse = await this.PlaidClientService.GetAccessTokenAsync(response.PlaidLink.PublicToken);
                var accessToken = accessTokenServiceResponse.access_token;
                response.PlaidLink.PlaidItemId = accessTokenServiceResponse.item_id;

                //// Get the name and logo
                var institutionDetails = await this.InstitutionRepository.GetInstitutionByIdAsync(response.InstitutionId);
                if (institutionDetails != null)
                {
                    response.Name = institutionDetails.InstitutionName;
                    response.Logo = institutionDetails.InstitutionLogo;
                }

                //// check if item already exist - For update item
                var updateItem = this.ItemRepository.GetItemByGivenItemId(response.PlaidLink.PlaidItemId);
                if (updateItem != null)
                {
                    return this.UpdatedLinkedItem(response, updateItem);
                }

                response = await this.GetIdentityAndAccounts(response, accessToken);
                var itemResult = this.ItemMapper(response, requestData, accessToken, response.PlaidLink.PlaidItemId);

                //// Insert into DB
                this.ItemRepository.AddItem(itemResult);

                //// Remove credit account information from the response not in DB
                response.PlaidLink.Accounts.RemoveAll(x => x.AccountType.Equals("credit"));
            }
            return response;
        }

        private async void GenericLinkedItem(IResponseItemCreate response, string institutionId, dynamic requestData)
        {
            if (response.IsLinked == true && response.PlaidLink.PublicToken != null)
            {
                var accessToken = requestData.access_token;
                response.PlaidLink.PlaidItemId = requestData.item_id;
                response.Name = requestData.InstitutionName;
                response.Logo = requestData.InstitutionLogo;

                //get identity               
                response = await this.GetIdentityAndAccounts(response, accessToken);
                var itemResult = this.ItemMapper(response, requestData, accessToken, response.PlaidLink.PlaidItemId);

                //// Insert into DB
                this.ItemRepository.AddItem(itemResult);

            }
        }

        /// <summary>
        /// UpdatedLinkedItem
        /// </summary>
        /// <param name="response">response</param>
        /// <param name="resultItem">resultItem</param>
        /// <returns>update response</returns>
        private IResponseItemCreate UpdatedLinkedItem(IResponseItemCreate response, IItem resultItem)
        {
            response.PlaidLink.Identity = resultItem.Identity;

            //// Remove islinked false and credit accounts
            resultItem.Accounts.RemoveAll(account => account.IsLinked == false || account.AccountType.Equals("credit"));
            var updatedAccounts = new List<Accounts>();

            //foreach (var account in resultItem.Accounts)
            //{
            //    Accounts updatedAccount = response.PlaidLink.Accounts.FirstOrDefault(x => x.PlaidAccountId == account.PlaidAccountId);
            //    account.Balances = updatedAccount.Balances;
            //    var acknowledgement = this.ItemRepository.UpdateAccountBalancesGivenAccountId(resultItem.EntityId, account.PlaidAccountId, account.Balances);
            //    updatedAccounts.Add(account);
            //}

            response.PlaidLink.Accounts = updatedAccounts;
            return response;
        }

        /// <summary>
        /// GetIdentityAndAccounts
        /// </summary>
        /// <param name="response">response</param>
        /// <param name="accessToken">accessToken</param>
        /// <returns>IResponseItemCreate response</returns>
        private async Task<IResponseItemCreate> GetIdentityAndAccounts(IResponseItemCreate response, string accessToken)
        {
            //// Identity and account data
            var identityResponse = await this.PlaidClientService.GetIdentityAsync(accessToken);
            if (identityResponse != null && identityResponse.accounts != null)
            {
                response.PlaidLink.Identity = identityResponse.identity;
                response.PlaidLink.Accounts = identityResponse.accounts;
                foreach (var account in response.PlaidLink.Accounts)
                {
                    account.OwnerName = string.Join(",", identityResponse.identity.names.Select(x => " " + x));
                }
            }

            //// Credit type linked false
            if (response.PlaidLink.Accounts != null)
            {
                response.PlaidLink.Accounts.Where(x => x.AccountType.Equals("credit")).ToList().ForEach(y => y.IsLinked = false);
            }
            return response;
        }
        public async Task PlaidLinkClientData(object data, EventInfo eventInfo)
        {
            Logger.Info($"Started processing for #{eventInfo.Name} event");
            var eventConfiguration = Configuration.events.Where(e => e.Name.Equals(eventInfo.Name)).First();
            if (eventConfiguration == null)
                throw new ArgumentException($"{nameof(eventConfiguration)}");
            //string responseData = eventConfiguration.Response.FormatWith(eventInfo);
            //object data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData);
            var ruleResult = DecisionEngineService.Execute<dynamic, ClientTrack>(eventConfiguration.Rule, new { input = data });
            await PlaidClientDataEventHandler(ruleResult);
        }
        // public async Task ProcessEvent(EventInfo eventInfo)
        public async Task ProcessEvent(object data, EventInfo eventInfo)
        {
            Logger.Info($"Started processing for #{eventInfo.Name} event");
            var eventConfiguration = Configuration.events.Where(e => e.Name.Equals(eventInfo.Name)).First();
            if (eventConfiguration == null)
                throw new ArgumentException($"{nameof(eventConfiguration)}");
            //string responseData = eventConfiguration.Response.FormatWith(eventInfo);
            //object data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData);
            var ruleResult = DecisionEngineService.Execute<dynamic, CashflowAutomaticRequest>(eventConfiguration.Rule, new { input = data });
            var result = await CalculateCashFlowEventHandler(ruleResult);
            if (eventConfiguration.CompletionEvents != null && eventConfiguration.CompletionEvents.Count > 0)
            {
                foreach (var eventname in eventConfiguration.CompletionEvents)
                {
                    await EventHub.Publish(eventname, new
                    {
                        EntityId = ruleResult?.EntityId,
                        EntityType = ruleResult?.EntityType,
                        Response = data,
                        Request = data,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                    Logger.Info($"Event #{eventname} published for #{ruleResult.EntityId}");
                }
            }
            Logger.Info($"Processing event #{eventInfo.Name} completed for {ruleResult?.EntityId} ");
        }
        public async Task PlaidClientDataEventHandler(IClientTrack ClientTrackRequest)
        {

            if (ClientTrackRequest == null)
                throw new ArgumentNullException(nameof(ClientTrackRequest));

            ClientTrackRequest.EntityType = EnsureEntityType(ClientTrackRequest.EntityType);

            if (String.IsNullOrWhiteSpace(ClientTrackRequest.EntityId))
                throw new ArgumentNullException(nameof(ClientTrackRequest.EntityId));
            IClientTrack clientTrack = new ClientTrack(ClientTrackRequest);
            var result = ClientTrackRepository.AddItem(clientTrack);
        }
        public async Task<TransactionResponse> CalculateCashFlowEventHandler(ICashflowAutomaticRequest cashflowAutomaticRequest)
        {
            TransactionResponse objPlaidResponse = new TransactionResponse();
            if (cashflowAutomaticRequest == null)
                throw new ArgumentNullException(nameof(cashflowAutomaticRequest));

            cashflowAutomaticRequest.EntityType = EnsureEntityType(cashflowAutomaticRequest.EntityType);

            if (String.IsNullOrWhiteSpace(cashflowAutomaticRequest.EntityId))
                throw new ArgumentNullException(nameof(cashflowAutomaticRequest.EntityId));

            PublicTokenExchangeRequest objExchangeTokenRequest = new PublicTokenExchangeRequest();
            objExchangeTokenRequest.Public_Token = cashflowAutomaticRequest.PublicToken;

            var objPlaidExchangeTokenResponse = await PlaidClientService.GetAccessTokenAsync(objExchangeTokenRequest.Public_Token);
            if (objPlaidExchangeTokenResponse.access_token == null)
            {
                throw new ArgumentNullException("Get Access Token :" + nameof(objPlaidExchangeTokenResponse.access_token) + "reason can be public token expired or Invalid");
            }

            if (objPlaidExchangeTokenResponse != null && objPlaidExchangeTokenResponse.access_token != null)
            {
                await EventHub.Publish(new PlaidGetExchangeTokenRequested
                {
                    EntityId = cashflowAutomaticRequest.EntityId,
                    EntityType = cashflowAutomaticRequest.EntityType,
                    Response = objPlaidExchangeTokenResponse,
                    Request = objExchangeTokenRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                AccountsRequest objAccountRequest = new AccountsRequest();
                objAccountRequest.AccessToken = objPlaidExchangeTokenResponse.access_token;
                if (Configuration.UseSimulation)
                {
                    objAccountRequest.AccessToken = cashflowAutomaticRequest.InstitutionId;
                }
                objAccountRequest.BankSupportedProductType = cashflowAutomaticRequest.BankSupportedProductType;
                objAccountRequest.SelectedAccountId = cashflowAutomaticRequest.AccountId;
                if (Configuration.AsyncTransactionStep == true)
                {
                    objPlaidResponse = await CalculateCashFlowInSteps(cashflowAutomaticRequest.EntityType, cashflowAutomaticRequest.EntityId, objAccountRequest, objExchangeTokenRequest.Public_Token);
                }
                else
                {
                    objPlaidResponse = await CalculateCashFlow(cashflowAutomaticRequest.EntityType, cashflowAutomaticRequest.EntityId, objAccountRequest, objExchangeTokenRequest.Public_Token);
                }
            }
            return objPlaidResponse;
        }
        public async Task<TransactionResponse> CalculateCashFlowInSteps(string entityType, string entityId, IAccountsRequest accountsRequest, string publicToken)
        {
            Logger.Info("Started CalculateCashFlowInSteps : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
            entityType = EnsureEntityType(entityType);
            if (String.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (accountsRequest == null)
                throw new ArgumentNullException(nameof(accountsRequest));

            if (string.IsNullOrEmpty(accountsRequest.AccessToken))
                throw new ArgumentNullException(nameof(accountsRequest.AccessToken));
            if (string.IsNullOrEmpty(accountsRequest.BankSupportedProductType))
                throw new ArgumentNullException(nameof(accountsRequest.BankSupportedProductType));
            TransactionResponse result = null;

            try
            {
                Logger.Info("Started plaid accounts and trans : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                var proxyRequest = new PlaidRequestTransaction(accountsRequest);
                var response = PlaidClientService.PullTransactionAsync(proxyRequest).Result;

                Logger.Info("Ended plaid accounts and trans : " + " EntityId:" + entityId + " at : " + TenantTime.Now + " account-count: " + response);
                if (response == null || response.accounts == null || response.accounts.Count < 0)
                {
                    Logger.Error("Error in PullTransactionAsync: no result for Transaction and Account found");
                    throw new Exception("Pull Transaction And Account: no result for Transaction and Account found" + response?.ErrorMessage + response?.PLAIDResponse);
                }

                if (response != null && response.accounts != null)
                    Logger.Info("Total Accounts from raw response : " + response.accounts.Count + " EntityId:" + entityId + " at : " + TenantTime.Now);

                result = new TransactionResponse(response, false);
                if (result.accounts != null)
                    Logger.Info("Total Accounts : " + result.accounts.Count + " EntityId:" + entityId + " at : " + TenantTime.Now);

                #region Save data in persistence
                IInstitutionsUpdateResponse InstitutionsData = new InstitutionsUpdateResponse();
                List<IAccountBulkResponse> lstAccounts = new List<IAccountBulkResponse>();
                var payloadAccount = new { Accounts = result.accounts, requestId = response.request_id, totalTransactions = response.total_transactions, Items = response.item, SelectedAccountId = accountsRequest.SelectedAccountId, UseSimulation = Configuration.UseSimulation };
                Logger.Info("Started DE rule ExtractPlaidV2AccountSyndicationData : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                var ruleAccountResult = DecisionEngineService.Execute<dynamic>("ExtractPlaidV2AccountSyndicationData", new { input = payloadAccount });

                Logger.Info("Ended DE rule ExtractPlaidV2AccountSyndicationData : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                if (ruleAccountResult != null)
                {
                    IBankRequestAccounts objAccounts = ExecuteRequest<BankRequestAccounts>(ruleAccountResult + "");

                    #region Save Accounts
                    try
                    {
                        if (objAccounts != null && objAccounts.Accounts != null && objAccounts.Accounts.Count > 0)
                        {
                            int accountBatchCount = Configuration.AccountBatchCount;
                            string bankName = objAccounts.Accounts[0].BankName;
                            try
                            {
                                Logger.Info("Get bank name of : " + objAccounts.Accounts[0].BankName + " EntityId:" + entityId + " at : " + TenantTime.Now);

                                InstitutionsData = PlaidClientService.InstitutionsGetByIdAsync(objAccounts.Accounts[0].BankName).Result;
                                bankName = InstitutionsData.institution.InstitutionName;

                                Logger.Info("Received bank name : " + bankName + " EntityId:" + entityId + " at : " + TenantTime.Now);
                            }
                            catch (Exception ex)
                            {
                                Logger.Error("Error in GetBank : " + " EntityId:" + entityId + " exception: " + ex.Message);
                            }
                            if (Configuration.EnableBulkAccount)
                            {
                                //take all the transactions at a time
                                accountBatchCount = objAccounts.Accounts.Count;
                            }
                            for (int i = 0; i < objAccounts.Accounts.Count; i = i + accountBatchCount)
                            {
                                Logger.Info("Save accounts i: " + i + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                var objAccountsBatch = objAccounts.Accounts.Skip(i).Take(accountBatchCount).ToList();
                                IBulkAccountRequest objAccountRequest = new BulkAccountRequest();
                                objAccountRequest.bankAccount = new List<IBankAccount>();
                                objAccountsBatch.ForEach(account =>
                                {
                                    account.BalanceAsOfDate = new TimeBucket(TenantTime.Now.UtcDateTime);
                                    account.EntityId = entityId;
                                    account.EntityType = entityType;
                                    account.CreatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
                                    account.BankName = bankName;
                                    account.AccessToken = accountsRequest.AccessToken;
                                });
                                objAccountRequest.bankAccount = objAccountsBatch;
                                Logger.Info("Save bulk accounts started i: " + i + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                var objAccountResponse = await CashflowService.AddBankAccountBulk(entityType, entityId, objAccountRequest);
                                if (objAccountResponse != null && objAccountResponse.bankAccounts != null && objAccountResponse.bankAccounts.Count > 0)
                                {
                                    Logger.Info("Save bulk accounts ended i: " + i + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                    if (Configuration.EnableBulkAccount)
                                    {
                                        lstAccounts.AddRange(objAccountResponse.bankAccounts);
                                    }
                                    else
                                    {
                                        if (i == (objAccounts.Accounts.Count - 1))
                                        {
                                            lstAccounts.AddRange(objAccountResponse.bankAccounts);
                                        }
                                    }
                                }
                                else
                                {
                                    Logger.Info("Save bulk accounts Failed cashflow service not running i: " + i + " EntityId:" + entityId + " at : " + TenantTime.Now);

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Error in add account at:" + TenantTime.Now + " EntityId:" + entityId + " Exception :" + ex.Message + ex.StackTrace);
                    }
                    #endregion
                }
                #endregion
                GetTransactions(entityType, entityId, accountsRequest, lstAccounts);
                if (response != null)
                {
                    IResponseItemCreate responseItemCreate = new ResponseItemCreate();
                    responseItemCreate.InstitutionId = response.item.institution_id;
                    responseItemCreate.PlaidLink = new PlaidLink { PublicToken = publicToken };
                    responseItemCreate.IsLinked = true;
                    dynamic requestData = new
                    {
                        ApplicantId = entityId,
                        InstitutionId = response.item.institution_id,
                        access_token = proxyRequest.AccessToken,
                        item_id = response.item.item_id,
                        InstitutionName = InstitutionsData.institution.InstitutionName,
                        InstitutionLogo = InstitutionsData.institution.InstitutionLogo
                    };
                    GenericLinkedItem(responseItemCreate, response.item.institution_id, requestData);
                }
                await EventHub.Publish(new PlaidSaveProductTypeRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = accountsRequest,
                    Request = accountsRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
            catch (Exception exception)
            {
                Logger.Error("Error in CalculateCashFlow (CalculateCashFlowInSteps) at:" + TenantTime.Now + " EntityId:" + entityId + " Exception :" + exception.Message + exception.StackTrace);
                await EventHub.Publish(new PlaidCalculateCashFlowRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = accountsRequest,
                    ReferenceNumber = null
                });
                await EventHub.Publish(new PlaidSaveProductTypeRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = accountsRequest,
                    ReferenceNumber = null
                });
                throw;
            }
            Logger.Info("Ended CalculateCashFlowInSteps : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
            return result;
        }
        private async void GetTransactions(string entityType, string entityId, IAccountsRequest accountsRequest, List<IAccountBulkResponse> lstAccounts)
        {
            System.Threading.Thread.Sleep(1000 * 60 * Configuration.WaitDurationForTransaction);
            ITransactionResponse result = null;
            try
            {
                int offset = 0;
                int count = Configuration.PageCount;
                int totalFetchedTransaction = 0;
                Logger.Info("Started GetTransactions : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                var proxyRequest = new Plaid.PlaidRequestTransaction(accountsRequest);
                do
                {
                    var transactionResponse = PlaidClientService.PullTransactionAsyncWithPagination(proxyRequest, offset, count).Result;
                    var transactionResult = new TransactionResponse(transactionResponse, true);
                    Logger.Info("total transaction by response total_transaction for all Account : " + transactionResponse.total_transactions);
                    if (result == null)
                    {
                        result = new TransactionResponse(transactionResponse, true);
                    }
                    else
                    {
                        result.transactions.AddRange(transactionResult.transactions);
                    }
                    offset = offset + Configuration.PageCount;
                    if (count > (result.total_transactions - result.transactions.Count))
                    {
                        count = result.total_transactions - result.transactions.Count;
                    }

                    // if (transactionResponse.accounts != null)
                    //     Logger.Info ("Total Accounts from raw response in GetTransactions : " + transactionResponse.accounts.Count + " EntityId:" + entityId + " at : " + TenantTime.Now);
                    if (transactionResponse.transactions != null)
                        Logger.Info("Total Transaction from raw response in PullTransaction Async With Pagination : " + transactionResponse.transactions.Count + "pagination offset" + offset + " EntityId:" + entityId + " at : " + TenantTime.Now);

                } while (result.transactions.Count < result.total_transactions);
                Logger.Info("Ended GetTransactions : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                if (result.accounts != null)
                    Logger.Info("Total Accounts in GetTransactions : " + result.accounts.Count + " EntityId:" + entityId + " at : " + TenantTime.Now);
                if (result.transactions != null)
                    Logger.Info("Total Transactions in GetTransactions : " + result.transactions.Count + " EntityId:" + entityId + " at : " + TenantTime.Now);
                #region Save Transactions

                if (result.transactions != null && result.transactions.Count > 0)
                {
                    var payloadTransaction = new { Transactions = result.transactions };
                    Logger.Info("Started DE rule ExtractPlaidV2TransactionSyndicationData : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                    var lstTransactions = DecisionEngineService.Execute<dynamic, BankTransactions>("ExtractPlaidV2TransactionSyndicationData", new { input = payloadTransaction });
                    Logger.Info("Ended DE rule ExtractPlaidV2TransactionSyndicationData : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                    if (lstTransactions != null && lstAccounts != null && lstAccounts.Count > 0)
                    {
                        foreach (var account in lstAccounts)
                        {
                            try
                            {                                
                                var objAccountTransaction = lstTransactions.Transactions.Where(t => t.ProviderAccountId == account.ProviderAccountId).ToList();
                                Logger.Info("Accounts loop started for account : " + account.ProviderAccountId + "and total transaction of it " + objAccountTransaction.Count + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                if (objAccountTransaction != null && objAccountTransaction.Count > 0)
                                {                                    
                                    totalFetchedTransaction = objAccountTransaction.Count;
                                    int batchCount = Configuration.TransactionBatchCount;
                                    if (Configuration.EnableBulkTransaction)
                                    {
                                        //take all the transactions at once
                                        batchCount = objAccountTransaction.Count;
                                    }
                                    var transactionRequest = new BulkTransactionRequest();
                                    transactionRequest.bankTransaction = new List<CapitalAlliance.Cashflow.ITransaction>();                                    
                                    for (int i = 0; i < objAccountTransaction.Count; i = i + batchCount)
                                    {
                                        var objTransactionBatch = objAccountTransaction.Skip(i).Take(batchCount).ToList();
                                       
                                        if (objTransactionBatch != null && objTransactionBatch.Count > 0)
                                        {
                                            try
                                            {
                                                Logger.Info("Transaction loop started for i : " + i + " EntityId:" + entityId + " at: " + TenantTime.Now);                                              
                                                foreach (var item in objTransactionBatch)
                                                {
                                                    item.AccountId = account.AccountId;
                                                    item.EntityId = entityId;
                                                    item.EntityType = entityType;
                                                    item.CreatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
                                                    if (!string.IsNullOrEmpty(item.TransactionDate))
                                                    {
                                                        var TransactionDate_OffSet = Convert.ToDateTime(item.TransactionDate);
                                                        item.TransactionOn = new TimeBucket(TransactionDate_OffSet);
                                                    }
                                                    transactionRequest.bankTransaction.Add(item);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Logger.Error("Error while processing in transaction for each loop at:" + TenantTime.Now + " EntityId:" + entityId + " AccountId:" + account.AccountId + " Exception :" + ex.Message + ex.StackTrace);
                                            }                                                                                                                              
                                        }
                                    }
                                            var RetryDataAttempt = new Dictionary<string, int>();
                                            RetryDataAttempt.Add(account.AccountId, 0);
                                            if (transactionRequest.bankTransaction.Any())
                                            {
                                                Logger.Info("SaveTransaction Per Account And Verification Started !!! For AccountId: " + account.AccountId + "EntityId:" + entityId + " at : " + TenantTime.Now);
                                                await SaveTransactionPerAccountAndVerification(entityType, entityId, transactionRequest, account.AccountId, batchCount, totalFetchedTransaction, RetryDataAttempt);
                                                Logger.Info("Ended SaveTransaction Per Account And Verification Started !!! For AccountId: " + account.AccountId + "EntityId:" + entityId + " at : " + TenantTime.Now);
                                            }
                                            else
                                            {
                                                Logger.Info("No Transaction found for AccountId :" + account.ProviderAccountId + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                            }   
                                }

                                #region Calculate Cashflow

                                bool IsSelected = false;
                                if (accountsRequest.SelectedAccountId == account.ProviderAccountId)
                                {
                                    IsSelected = true;
                                }

                                await EventHub.Publish("ExtractAccountsAndTransactions", new
                                {
                                    EntityId = entityId,
                                    EntityType = entityType,
                                    Response = new { AccountId = account.AccountId, IsSelected = IsSelected },
                                    Request = new { AccountId = account.AccountId },
                                    ReferenceNumber = Guid.NewGuid().ToString("N")
                                });
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                Logger.Error("Error while processing in account loop at:" + TenantTime.Now + " EntityId:" + entityId + " AccountId:" + account + " Exception :" + ex.Message + ex.StackTrace);
                            }
                        }
                    }
                }
                else
                {
                    Logger.Info("No transactions available at:" + TenantTime.Now + " EntityId:" + entityId);
                    foreach (var account in lstAccounts)
                    {
                        await EventHub.Publish("ExtractAccountsAndTransactions", new
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = new { AccountId = account.AccountId, IsSelected = false },
                            Request = new { AccountId = account.AccountId },
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetTransactions at:" + TenantTime.Now + " EntityId:" + entityId + " Exception :" + ex.Message + ex.StackTrace);
            }
        }

        private async Task SaveTransactionPerAccountAndVerification(string entityType, string entityId, IBulkTransactionRequest transactionRequest, string accountId, int batchCount, int totalFetchedTransaction, Dictionary<string, int> retryDataAttempt)
        {
            try
            {
                await CashflowService.AddBankTransaction(entityType, entityId, transactionRequest);
                if (retryDataAttempt[accountId] < Configuration.MaxRetry)
                {                    
                    bool verificationResult = CashflowService.VerifyTransactionsCount(entityType, entityId,
                        accountId, totalFetchedTransaction).Result;
                    
                    if (!verificationResult)
                    {
                        Logger.Info("Cross Verification Failed" + entityId + " at : " + TenantTime.Now + "retry Attempting :" + retryDataAttempt[accountId] + " for AccountId :" + accountId);
                        if (retryDataAttempt.ContainsKey(accountId))
                        {
                            retryDataAttempt[accountId] += 1;
                        }
                        await SaveTransactionPerAccountAndVerification(entityType, entityId, transactionRequest, accountId, batchCount, totalFetchedTransaction, retryDataAttempt);
                    }
                }
            }
            catch (Exception ex)
            {
                if (retryDataAttempt[accountId] < Configuration.MaxRetry)
                {
                    if (retryDataAttempt.ContainsKey(accountId))
                    {
                        retryDataAttempt[accountId] += 1;
                    }
                    await SaveTransactionPerAccountAndVerification(entityType, entityId, transactionRequest, accountId, batchCount, totalFetchedTransaction, retryDataAttempt);
                }
                Logger.Error("Error while processing SaveTransactionPerAccountAndVerification:" + TenantTime.Now + " EntityId:" + entityId + " AccountId:" + accountId + " Exception :" + ex.Message + ex.StackTrace);
            }
        }
        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response, exception);
            }
        }
        public async Task<TransactionResponse> CalculateCashFlow(string entityType, string entityId, IAccountsRequest accountsRequest, string publicToken)
        {
            Logger.Info("Started CalculateCashFlow : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
            entityType = EnsureEntityType(entityType);
            if (String.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (accountsRequest == null)
                throw new ArgumentNullException(nameof(accountsRequest));

            if (string.IsNullOrEmpty(accountsRequest.AccessToken))
                throw new ArgumentNullException(nameof(accountsRequest.AccessToken));

            if (string.IsNullOrEmpty(accountsRequest.BankSupportedProductType))
                throw new ArgumentNullException(nameof(accountsRequest.BankSupportedProductType));
            TransactionResponse result = null;

            try
            {
                Logger.Info("Started plaid accounts and trans : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                var proxyRequest = new PlaidRequestTransaction(accountsRequest);
                var response = await PlaidClientService.PullTransactionAsync(proxyRequest);
                Logger.Info("Ended plaid accounts and trans : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                if (response == null || response.accounts == null || response.accounts.Count < 0)
                {
                    Logger.Error("Error in PullTransactionAsync: no result for Transaction and Account found");
                    throw new Exception("Pull Transaction And Account: no result for Transaction and Account found" + response?.ErrorMessage + response?.PLAIDResponse);
                }

                if (response != null && response.accounts != null)
                    Logger.Info("Total Accounts from raw response : " + response.accounts.Count + " EntityId:" + entityId + " at : " + TenantTime.Now);

                result = new TransactionResponse(response);

                if (result.accounts != null)
                    Logger.Info("Total Accounts : " + result.accounts.Count + " EntityId:" + entityId + " at : " + TenantTime.Now);
                if (result.transactions != null)
                    Logger.Info("Total Transactions : " + result.transactions.Count + " EntityId:" + entityId + " at : " + TenantTime.Now);

                #region Save data in persistence

                List<IAccountBulkResponse> lstAccounts = new List<IAccountBulkResponse>();
                IInstitutionsUpdateResponse InstitutionsData = new InstitutionsUpdateResponse();
                var payloadAccount = new { Accounts = result.accounts, requestId = result.request_id, totalTransactions = result.total_transactions, Items = result.item, SelectedAccountId = accountsRequest.SelectedAccountId, UseSimulation = Configuration.UseSimulation };
                Logger.Info("Started DE rule ExtractPlaidV2AccountSyndicationData : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                var ruleAccountResult = DecisionEngineService.Execute<dynamic>("ExtractPlaidV2AccountSyndicationData", new { input = payloadAccount });
                Logger.Info("Ended DE rule ExtractPlaidV2AccountSyndicationData : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                if (ruleAccountResult != null)
                {
                    IBankRequestAccounts objAccounts = ExecuteRequest<BankRequestAccounts>(ruleAccountResult + "");

                    #region Save Accounts
                    try
                    {
                        if (objAccounts != null && objAccounts.Accounts != null && objAccounts.Accounts.Count > 0)
                        {
                            int accountBatchCount = Configuration.AccountBatchCount;
                            string bankName = objAccounts.Accounts[0].BankName;
                            try
                            {
                                Logger.Info("Get bank name of : " + objAccounts.Accounts[0].BankName + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                //bankName = Proxy.GetBank(objAccounts.Accounts[0].BankName).Name;
                                InstitutionsData = PlaidClientService.InstitutionsGetByIdAsync(objAccounts.Accounts[0].BankName).Result;
                                bankName = InstitutionsData.institution.InstitutionName;
                                Logger.Info("Received bank name : " + bankName + " EntityId:" + entityId + " at : " + TenantTime.Now);
                            }
                            catch (Exception ex)
                            {
                                Logger.Error("Error in GetBank : " + " EntityId:" + entityId + " exception: " + ex.Message);
                            }
                            if (Configuration.EnableBulkAccount)
                            {
                                //take all the transactions at a time
                                accountBatchCount = objAccounts.Accounts.Count;
                            }
                            for (int i = 0; i < objAccounts.Accounts.Count; i = i + accountBatchCount)
                            {
                                Logger.Info("Save accounts i: " + i + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                var objAccountsBatch = objAccounts.Accounts.Skip(i).Take(accountBatchCount).ToList();
                                IBulkAccountRequest objAccountRequest = new BulkAccountRequest();
                                objAccountRequest.bankAccount = new List<IBankAccount>();
                                objAccountsBatch.ForEach(account =>
                                {
                                    account.BalanceAsOfDate = new TimeBucket(TenantTime.Now.UtcDateTime);
                                    account.EntityId = entityId;
                                    account.EntityType = entityType;
                                    account.CreatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
                                    account.BankName = bankName;
                                    account.AccessToken = accountsRequest.AccessToken;
                                });
                                objAccountRequest.bankAccount = objAccountsBatch;
                                Logger.Info("Save bulk accounts started i: " + i + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                var objAccountResponse = await CashflowService.AddBankAccountBulk(entityType, entityId, objAccountRequest);
                                if (objAccountResponse != null && objAccountResponse.bankAccounts != null && objAccountResponse.bankAccounts.Count > 0)
                                {
                                    Logger.Info("Save bulk accounts ended i: " + i + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                    if (Configuration.EnableBulkAccount)
                                    {
                                        lstAccounts.AddRange(objAccountResponse.bankAccounts);
                                    }
                                    else
                                    {
                                        if (i == (objAccounts.Accounts.Count - 1))
                                        {
                                            lstAccounts.AddRange(objAccountResponse.bankAccounts);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Error in add account at:" + TenantTime.Now + " EntityId:" + entityId + " Exception :" + ex.Message + ex.StackTrace);
                    }
                    #endregion
                }

                #region Save Transactions

                if (result.transactions != null && result.transactions.Count > 0)
                {
                    var payloadTransaction = new { Transactions = result.transactions };
                    Logger.Info("Started DE rule ExtractPlaidV2TransactionSyndicationData : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                    var lstTransactions = DecisionEngineService.Execute<dynamic, BankTransactions>("ExtractPlaidV2TransactionSyndicationData", new { input = payloadTransaction });
                    Logger.Info("Ended DE rule ExtractPlaidV2TransactionSyndicationData : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
                    if (lstTransactions != null && lstAccounts != null && lstAccounts.Count > 0)
                    {
                        foreach (var account in lstAccounts)
                        {
                            Logger.Info("Accounts loop started for account : " + account.ProviderAccountId + " EntityId:" + entityId + " at : " + TenantTime.Now);
                            try
                            {
                                var objAccountTransaction = lstTransactions.Transactions.Where(t => t.ProviderAccountId == account.ProviderAccountId).ToList();
                                if (objAccountTransaction != null && objAccountTransaction.Count > 0)
                                {
                                    int batchCount = Configuration.TransactionBatchCount;
                                    if (Configuration.EnableBulkTransaction)
                                    {
                                        //take all the transactions at a time
                                        batchCount = objAccountTransaction.Count;
                                    }
                                    for (int i = 0; i < objAccountTransaction.Count; i = i + batchCount)
                                    {
                                        try
                                        {
                                            Logger.Info("Transaction loop started for i : " + i + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                            var objTransactionBatch = objAccountTransaction.Skip(i).Take(batchCount).ToList();
                                            var transactionRequest = new BulkTransactionRequest();
                                            if (objTransactionBatch != null && objTransactionBatch.Count > 0)
                                            {
                                                try
                                                {
                                                    transactionRequest.bankTransaction = new List<CapitalAlliance.Cashflow.ITransaction>();
                                                    foreach (var item in objTransactionBatch)
                                                    {
                                                        item.AccountId = account.AccountId;
                                                        item.EntityId = entityId;
                                                        item.EntityType = entityType;
                                                        item.CreatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
                                                        if (!string.IsNullOrEmpty(item.TransactionDate))
                                                        {
                                                            var TransactionDate_OffSet = Convert.ToDateTime(item.TransactionDate);
                                                            item.TransactionOn = new TimeBucket(TransactionDate_OffSet);
                                                        }
                                                        transactionRequest.bankTransaction.Add(item);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error("Error while processing in transaction for each loop at:" + TenantTime.Now + " EntityId:" + entityId + " AccountId:" + account + " Exception :" + ex.Message + ex.StackTrace);
                                                }
                                                Logger.Info("Started bulk transaction add for i : " + i + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                                await CashflowService.AddBankTransaction(entityType, entityId, transactionRequest);
                                                Logger.Info("Ended bulk transaction add for i : " + i + " EntityId:" + entityId + " at : " + TenantTime.Now);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error("Error while processing in transaction loop at:" + TenantTime.Now + " EntityId:" + entityId + " AccountId:" + account + " Exception :" + ex.Message + ex.StackTrace);
                                        }
                                    }
                                }

                                #region Calculate Cashflow

                                bool IsSelected = false;
                                if (accountsRequest.SelectedAccountId == account.ProviderAccountId)
                                {
                                    IsSelected = true;
                                }

                                await EventHub.Publish("ExtractAccountsAndTransactions", new
                                {
                                    EntityId = entityId,
                                    EntityType = entityType,
                                    Response = new { AccountId = account.AccountId, IsSelected = IsSelected },
                                    Request = new { AccountId = account.AccountId },
                                    ReferenceNumber = Guid.NewGuid().ToString("N")
                                });
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                Logger.Error("Error while processing in account loop at:" + TenantTime.Now + " EntityId:" + entityId + " AccountId:" + account + " Exception :" + ex.Message + ex.StackTrace);
                            }
                        }
                    }
                }
                else
                {
                    Logger.Info("No transactions available at:" + TenantTime.Now + " EntityId:" + entityId);
                    foreach (var account in lstAccounts)
                    {
                        await EventHub.Publish("ExtractAccountsAndTransactions", new
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = new { AccountId = account.AccountId, IsSelected = false },
                            Request = new { AccountId = account.AccountId },
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });
                    }
                }
                #endregion

                #endregion

                if (response != null)
                {
                    IResponseItemCreate responseItemCreate = new ResponseItemCreate();
                    responseItemCreate.InstitutionId = response.item.institution_id;
                    responseItemCreate.PlaidLink.PublicToken = publicToken;
                    responseItemCreate.IsLinked = true;
                    dynamic requestData = new
                    {
                        ApplicantId = entityId,
                        InstitutionId = response.item.institution_id,
                        access_token = proxyRequest.AccessToken,
                        item_id = response.item.item_id,
                        InstitutionName = InstitutionsData.institution.InstitutionName,
                        InstitutionLogo = InstitutionsData.institution.InstitutionLogo
                    };
                    GenericLinkedItem(responseItemCreate, response.item.institution_id, requestData);
                }

                await EventHub.Publish(new PlaidSaveProductTypeRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = accountsRequest,
                    Request = accountsRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
            catch (Exception exception)
            {
                Logger.Error("Error in CalculateCashFlow at:" + TenantTime.Now + " EntityId:" + entityId + " Exception :" + exception.Message + exception.StackTrace);
                await EventHub.Publish(new PlaidCalculateCashFlowRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = accountsRequest,
                    ReferenceNumber = null
                });
                await EventHub.Publish(new PlaidSaveProductTypeRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = accountsRequest,
                    ReferenceNumber = null
                });
                throw;
            }
            Logger.Info("Ended CalculateCashFlow : " + " EntityId:" + entityId + " at : " + TenantTime.Now);
            return result;
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}