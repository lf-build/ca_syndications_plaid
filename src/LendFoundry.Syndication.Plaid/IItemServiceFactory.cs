﻿using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Plaid
{
    public interface IItemServiceFactory
    {
        IItemService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
