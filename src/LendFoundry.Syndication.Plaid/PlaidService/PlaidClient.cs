﻿using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Plaid
{
    public class PlaidClient : IPlaidClient
    {
        public PlaidClient(
            ILogger logger,
            IConfiguration configuration)
        {
            CommandExecutor = new CommandExecutor(logger);
            Configuration = configuration;
            Logger = logger;
            if (Configuration == null)
                throw new ArgumentException("Plaid configuration cannot be found, please check");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            client = new RestClient(Configuration.UrlSandbox);
        }
        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        private ILogger Logger { get; }

        /// <summary>
        /// Gets CommandExecutor
        /// </summary>
        private CommandExecutor CommandExecutor { get; }
        private IRestClient client { get; }

        /// <summary>
        /// CreateItemExecutionFlow
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IResponseItemCreate> CreateItemExecutionFlow(IRequestItemCreate requestData)
        {
            IResponseItemCreate response = new ResponseItemCreate();
            response.InstitutionId = requestData.InstitutionId;

            var createItemRequest = CreatingItemModel(requestData);
            var request = new RestRequest("link/item/create", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(createItemRequest);
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var error = JsonConvert.DeserializeObject<PlaidResponseError>(responseResult.Content);
                response.HasError = true;
                response.Error = error;

                return response;
            }
            else if (responseResult.StatusCode.ToString().Equals("210"))
            {
                var mfa = JsonConvert.DeserializeObject<PlaidMfa>(responseResult.Content);
                response.HasMfa = true;
                response.Mfa = mfa;

                return response;
            }
            else
            {
                var plaidLink = JsonConvert.DeserializeObject<PlaidLink>(responseResult.Content);
                response.IsLinked = true;
                response.PlaidLink = plaidLink;

                return response;
            }
        }

        /// <summary>
        /// Item Delete
        /// </summary>
        /// <param name="request">request body</param>
        /// <returns>boolean value</returns>
        public async Task<IList<PlaidResponseItemDelete>> ItemDeleteAsync(IEnumerable<string> accessTokens)
        {
            IList<PlaidResponseItemDelete> result = new List<PlaidResponseItemDelete>();
            foreach (var at in accessTokens)
            {
                var response = new PlaidResponseItemDelete();
                var accessTokenId = at.Substring(15, 8);
                var request = new RestRequest("item/delete", Method.POST);
                request.JsonSerializer = new NewtonsoftJsonSerializer();
                request.AddJsonBody(new PlaidRequestItemDelete()
                {
                    client_id = Configuration.ClientId,
                    secret = Configuration.Secret,
                    access_token = at
                });
                TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
                RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
                IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

                if (responseResult.StatusCode == HttpStatusCode.BadRequest)
                {
                    response.Deleted = false;
                    response.error = JsonConvert.DeserializeObject<PlaidResponseError>(responseResult.Content);
                    response.AccessToken = accessTokenId;
                }
                else if (responseResult.StatusCode == HttpStatusCode.OK)
                {
                    response.Deleted = true;
                    response = JsonConvert.DeserializeObject<PlaidResponseItemDelete>(responseResult.Content);
                    response.AccessToken = accessTokenId;
                }
                result.Add(response);
            }
            return result;
        }

        /// <summary>
        /// Item Delete
        /// </summary>
        /// <param name="request">request body</param>
        /// <returns>boolean value</returns>
        public async Task<IPlaidResponseItemDelete> ItemDeleteNewAsync(string accessToken)
        {
            IPlaidResponseItemDelete returnResult = new PlaidResponseItemDelete();

            var request = new RestRequest("item/delete", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(new PlaidRequestItemDelete()
            {
                client_id = Configuration.ClientId,
                secret = Configuration.Secret,
                access_token = accessToken
            });
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == HttpStatusCode.BadRequest)
            {
                returnResult.Deleted = false;
                returnResult.error = JsonConvert.DeserializeObject<PlaidResponseError>(responseResult.Content);
                returnResult.AccessToken = accessToken;
            }
            else if (responseResult.StatusCode == HttpStatusCode.OK)
            {
                returnResult.Deleted = true;
                returnResult = JsonConvert.DeserializeObject<PlaidResponseItemDelete>(responseResult.Content);
                returnResult.AccessToken = accessToken;
            }
            return returnResult;
        }

        /// <summary>
        /// CreateMfaExecutionFlow
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IResponseItemCreate> CreateMfaExecutionFlow(IMFADocittRequest requestData)
        {
            IResponseItemCreate response = new ResponseItemCreate();
            response.InstitutionId = requestData.InstitutionId;


            var mfaRequestData = this.CreatingMfaModel(requestData);
            var request = new RestRequest("link/item/mfa", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(mfaRequestData);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();

            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var error = JsonConvert.DeserializeObject<PlaidResponseError>(responseResult.Content);
                response.HasError = true;
                response.Error = error;
                return response;
            }
            else if (responseResult.StatusCode.ToString().Equals("210"))
            {
                var mfa = JsonConvert.DeserializeObject<PlaidMfa>(responseResult.Content);
                response.HasMfa = true;
                response.Mfa = mfa;
                if (requestData.PublicToken != null)
                    response.Mfa.PublicToken = requestData.PublicToken;
                return response;
            }

            else
            {
                var plaidLink = JsonConvert.DeserializeObject<PlaidLink>(responseResult.Content);
                response.IsLinked = true;
                response.PlaidLink = plaidLink;
                if (response.PlaidLink.PublicToken == null)
                    response.PlaidLink.PublicToken = requestData.PublicToken;
                return response;
            }


        }

        /// <summary>
        /// PullTransactionAsync
        /// </summary>
        /// <param name="transactionRequest">transactionRequest</param>
        /// <returns>Transaction Response</returns>
        public async Task<TransactionResponse> PullTransactionAsync(IPlaidRequestTransaction transactionRequest)
        {
            var sclient = new RestClient();
            var request = new RestRequest("transactions/get", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            if (Configuration.UseSimulation == true)
            {
                AddDefaultParameters(ref request);
                request.AddParameter("access_token", transactionRequest.AccessToken);
            }
            else
            {   
                var body = new PlaidRequestTransaction();
                body.ClientId = Configuration.ClientId;
                body.Secret = Configuration.Secret;
                body.AccessToken = transactionRequest.AccessToken;
                if (Configuration.TransactionDays > 0)
                {
                    Options1 options = new Options1();
                    options.Count = Configuration.PageCount;
                    options.Offset = 0;
                    body.StartDate = DateTime.UtcNow.AddDays(Configuration.TransactionDays * -1).ToString("yyyy-MM-dd");
                    body.EndDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
                    body.Options = options;
                }
                request.AddJsonBody(body);
            }
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            if (Configuration.UseSimulation)
            {
                var baseUri = new Uri(Configuration.SimulationUrl);
                sclient = new RestClient(baseUri);
                RestRequestAsyncHandle handle = sclient.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            }
            else
            {
                RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            }
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);
            if (responseResult.ErrorException != null)
                throw new PlaidException("Service call failed", responseResult.ErrorException);

            if (responseResult.ResponseStatus != ResponseStatus.Completed)
                throw new PlaidException(
                    $"Service call failed. Status {responseResult.ResponseStatus}. Response: {responseResult.ErrorMessage ?? ""}");

            if (responseResult.StatusCode.ToString().ToLower() == "method not allowed")
                throw new PlaidException(
                    $"Service call failed. Status {responseResult.ResponseStatus}. Response: {responseResult.StatusDescription ?? ""}");
            if (responseResult.StatusCode == HttpStatusCode.NotFound)
                throw new PlaidException(responseResult.Content);

            if (responseResult.StatusCode != HttpStatusCode.OK)
                throw new PlaidException(
                    $"Service call failed. Status {responseResult.StatusCode}. Response: {responseResult.ErrorMessage ?? ""}");

            try
            {
                return JsonConvert.DeserializeObject<TransactionResponse>(responseResult.Content);
              
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to Deserialize:" + responseResult.ErrorMessage, exception);
            }

        }
        /// <summary>
        /// Pull Transaction With Pagination
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public async Task<TransactionResponse> PullTransactionAsyncWithPagination(IPlaidRequestTransaction transactionRequest, int offset = 0, int count = 0)
        {
            var sclient = new RestClient();
            var request = new RestRequest("transactions/get", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            if (Configuration.UseSimulation == true)
            {
                AddDefaultParameters(ref request);
                request.AddParameter("access_token", transactionRequest.AccessToken);
            }
            else
            {   //request.AddJsonBody(transactionRequest);
                var body = new PlaidRequestTransaction();
                body.ClientId = Configuration.ClientId;
                body.Secret = Configuration.Secret;
                body.AccessToken = transactionRequest.AccessToken;
                if (Configuration.TransactionDays > 0)
                {
                    Options1 options = new Options1();
                    options.Count = count;
                    options.Offset = offset;
                    body.StartDate = DateTime.UtcNow.AddDays(Configuration.TransactionDays * -1).ToString("yyyy-MM-dd");
                    body.EndDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
                    body.Options = options;
                }
                request.AddJsonBody(body);
            }
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            if (Configuration.UseSimulation)
            {
                var baseUri = new Uri(Configuration.SimulationUrl);
                sclient = new RestClient(baseUri);
                RestRequestAsyncHandle handle = sclient.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            }
            else
            {
                RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            }
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);
            if (responseResult.ErrorException != null)
                throw new PlaidException("Service call failed", responseResult.ErrorException);

            if (responseResult.ResponseStatus != ResponseStatus.Completed)
                throw new PlaidException(
                    $"Service call failed. Status {responseResult.ResponseStatus}. Response: {responseResult.ErrorMessage ?? ""}");

            if (responseResult.StatusCode.ToString().ToLower() == "method not allowed")
                throw new PlaidException(
                    $"Service call failed. Status {responseResult.ResponseStatus}. Response: {responseResult.StatusDescription ?? ""}");
            if (responseResult.StatusCode == HttpStatusCode.NotFound)
                throw new PlaidException(responseResult.Content);

            if (responseResult.StatusCode != HttpStatusCode.OK)
                throw new PlaidException(
                    $"Service call failed. Status {responseResult.StatusCode}. Response: {responseResult.ErrorMessage ?? ""}");

            try
            {
                return JsonConvert.DeserializeObject<TransactionResponse>(responseResult.Content);

            }
            catch (Exception exception)
            {
                throw new Exception("Unable to Deserialize:" + responseResult.ErrorMessage, exception);
            }

        }

        private void AddDefaultParameters(ref RestRequest request)
        {
            request.AddParameter("client_id", Configuration.ClientId);
            request.AddParameter("secret", Configuration.Secret);
        }

        /// <summary>
        /// Get the list of Institutions from Plaid
        /// </summary>
        /// <param name="count">no of records</param>
        /// <param name="offset">starting from</param>
        /// <returns>list of institutions</returns>
        public async Task<IList<InstitutionsVM>> InstitutionsGetAsync(int count, int offset)
        {
            var institutionRequest = this.CreatingInstitutionModel(count, offset);
            var request = new RestRequest("institutions/get", Method.POST);

            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(institutionRequest);
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);
            var result = JsonConvert.DeserializeObject<InstitutionsResponse>(responseResult.Content);
            return result.institutions;
        }

        /// <summary>
        /// Get the list of Institutions from Plaid
        /// </summary>
        /// <param name="count">no of records</param>
        /// <param name="offset">starting from</param>
        /// <returns>list of institutions</returns>
        public async Task<InstitutionsUpdateResponse> InstitutionsGetByIdAsync(string institutionId)
        {
            var institutionRequest = this.CreatingSearchByIdInstitutionModel(institutionId);
            var request = new RestRequest("institutions/get_by_id", Method.POST);

            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(institutionRequest);
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);
            var result = JsonConvert.DeserializeObject<InstitutionsUpdateResponse>(responseResult.Content);
            return result;
        }

        /// <summary>
        /// Get total number of Institutions from Plaid
        /// </summary>
        /// <param name="count">no of records</param>
        /// <param name="offset">starting from</param>
        /// <returns>list of institutions</returns>
        public async Task<int> InstitutionsCountGetAsync(int count, int offset)
        {
            var institutionRequest = this.CreatingInstitutionModel(1, 0);
            var request = new RestRequest("institutions/get", Method.POST);

            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(institutionRequest);
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);
            var result = JsonConvert.DeserializeObject<InstitutionsResponse>(responseResult.Content);
            return result.total;
        }

        /// <summary>
        /// Get the list of institutions matching the search key
        /// </summary>
        /// <param name="searchKey">search key</param>
        /// <returns>list of institutions</returns>
        public async Task<IList<InstitutionsVM>> InstitutionsSearchAsync(string searchKey)
        {
            var institutionRequest = this.CreatingSearchInstitutionModel(searchKey);
            var request = new RestRequest("institutions/search", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(institutionRequest);
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);
            var result = JsonConvert.DeserializeObject<InstitutionsResponse>(responseResult.Content);
            return result.institutions;
        }

        //// Use for exchange public token to get access token , returns Access token , request Id and Item Id.
        /// <summary>
        /// CreateItemAsync
        /// </summary>
        /// <param name="client"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public async Task<PublicTokenExchangeResponse> GetAccessTokenAsync(string public_token)
        {

            var response = new PublicTokenExchangeResponse();
            //// Exchange token
            var exchangeTokenRequest = new PublicTokenExchangeRequest
            {
                Client_Id = Configuration.ClientId,
                Public_Token = public_token,
                Secret = Configuration.Secret
            };

            var request = new RestRequest("item/public_token/exchange", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(exchangeTokenRequest);
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();

            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);
            if (responseResult.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var error = JsonConvert.DeserializeObject<PlaidResponseError>(responseResult.Content);
                response.error = error;
                return response;
            }
            else
            {
                var result = JsonConvert.DeserializeObject<PublicTokenExchangeResponse>(responseResult.Content);
                return result;
            }

        }

        /// <summary>
        /// GetAccountAsync
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <returns>accounts data</returns>
        public async Task<IAccountsVM> GetAccountAsync(string accessToken)
        {
            var response = new AccountsVM();
            var request = new RestRequest("accounts/get", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            var accountRequest = this.CreatingCommonModel(accessToken);
            request.AddJsonBody(accountRequest);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();

            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var error = JsonConvert.DeserializeObject<PlaidResponseError>(responseResult.Content);
                response.HasError = true;
                response.Error = error;
                return response;
            }
            else
            {
                response = JsonConvert.DeserializeObject<AccountsVM>(responseResult.Content);
                return response;
            }
        }

        /// <summary>
        /// GetIdentityAsync
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <returns>identity data</returns>
        public async Task<IResponseIdentity> GetIdentityAsync(string accessToken)
        {
            var request = new RestRequest("identity/get", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            var identityRequest = this.CreatingCommonModel(accessToken);
            request.AddJsonBody(identityRequest);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            var result = JsonConvert.DeserializeObject<ResponseIdentity>(responseResult.Content);
            return result;
        }


        /// <summary>
        /// CreatingPublicTokenAsync
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <returns>return publictoke</returns>
        public async Task<string> CreatingPublicTokenAsync(string accessToken)
        {
            var publicTokenRequest = this.CreatingCommonModel(accessToken);
            var request = new RestRequest("item/public_token/create", Method.POST);

            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(publicTokenRequest);
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);
            var result = JsonConvert.DeserializeObject<PlaidCreatePublicTokenResponse>(responseResult.Content);
            return result.PublicToken;
        }


        public async Task<IResponseItemCreate> UpdateItemExecutionFlow(IRequestItemUpdate requestData, string publicToken)
        {
            IResponseItemCreate response = new ResponseItemCreate();
            response.InstitutionId = requestData.InstitutionId;


            var itemUpdateRequest = this.CreatingItemUpdateModel(requestData, publicToken);
            var request = new RestRequest("link/item/credentials/update", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddJsonBody(itemUpdateRequest);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();

            RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
            IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

            if (responseResult.StatusCode == HttpStatusCode.BadRequest)
            {
                var error = JsonConvert.DeserializeObject<PlaidResponseError>(responseResult.Content);
                response.HasError = true;
                response.Error = error;
                return response;
            }
            else if (responseResult.StatusCode.ToString().Equals("210"))
            {
                var mfa = JsonConvert.DeserializeObject<PlaidMfa>(responseResult.Content);
                response.HasMfa = true;
                response.Mfa = mfa;
                response.Mfa.PublicToken = publicToken;
                return response;
            }

            else
            {
                var plaidLink = JsonConvert.DeserializeObject<PlaidLink>(responseResult.Content);
                response.IsLinked = true;
                response.PlaidLink = plaidLink;
                response.PlaidLink.PublicToken = publicToken;
                return response;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <returns>common request model</returns>
        private ICommonRequest CreatingCommonModel(string accessToken)
        {
            return new CommonRequest
            {
                Access_Token = accessToken,
                Client_Id = this.Configuration.ClientId,
                Secret = this.Configuration.Secret
            };
        }

        /// <summary>
        /// CreatingInstitutionModel
        /// </summary>
        /// <returns>response</returns>
        private InstitutionRequest CreatingInstitutionModel(int count, int offset)
        {
            var institutionRequest = new InstitutionRequest
            {
                client_id = Configuration.ClientId,
                secret = Configuration.Secret,
                count = count,
                offset = offset
            };
            return institutionRequest;
        }

        /// <summary>
        /// CreatingInstitutionModel
        /// </summary>
        /// <returns>response</returns>
        private InstitutionSearchRequest CreatingSearchInstitutionModel(string search)
        {
            var institutionRequest = new InstitutionSearchRequest
            {
                public_key = Configuration.PublicKey,
                products = new string[] { "transactions" },
                options = new SearchOptions() { IncludeDisplayData = true },
                query = search
            };
            return institutionRequest;
        }

        /// <summary>
        /// CreatingInstitutionModel
        /// </summary>
        /// <returns>response</returns>
        private InstitutionSearchByIdRequest CreatingSearchByIdInstitutionModel(string institutionId)
        {
            var institutionRequest = new InstitutionSearchByIdRequest
            {
                public_key = Configuration.PublicKey,
                institution_id = institutionId,
                options = new SearchOptions() { IncludeDisplayData = true }

            };
            return institutionRequest;
        }

        private IPlaidRequestItemCreate CreatingItemModel(IRequestItemCreate requestData)
        {
            //// Create Item
            var createItemRequest = new PlaidRequestItemCreate
            {
                Credentials = new PlaidUserCredential
                {
                    Username = requestData.UserName,
                    Password = requestData.Password,
                    Pin = !string.IsNullOrWhiteSpace(requestData.Pin) ? requestData.Pin : null
                },
                Initial_Products = Configuration.InitialProducts,
                Institution_Id = requestData.InstitutionId,
                Options = new Options
                {
                    Webhook = Configuration.WebhookUrl
                },
                Public_Key = Configuration.PublicKey
            };

            return createItemRequest;
        }

        private IMFARequest CreatingMfaModel(IMFADocittRequest requestData)
        {
            //// Create Item
            var mfaRequest = new MFARequest
            {
                Public_Key = Configuration.PublicKey,
                Mfa_Type = requestData.MfaType,
                Public_Token = requestData.PublicToken,
                Responses = requestData.Responses
            };

            return mfaRequest;
        }

        /// <summary>
        /// CreatingItemUpdateModel
        /// </summary>
        /// <param name="requestData">requestData</param>
        /// <param name="publicToken">publicToken</param>
        /// <returns>Plaid Request update model</returns>
        private IPlaidRequestItemUpdate CreatingItemUpdateModel(IRequestItemUpdate requestData, string publicToken)
        {
            //// Update Item
            return new PlaidRequestItemUpdate
            {
                Credentials = new PlaidUserCredential
                {
                    Username = requestData.UserName,
                    Password = requestData.Password,
                    Pin = !string.IsNullOrWhiteSpace(requestData.Pin) ? requestData.Pin : null
                },
                PublicToken = publicToken,
                Public_Key = Configuration.PublicKey
            };
        }
    }
}