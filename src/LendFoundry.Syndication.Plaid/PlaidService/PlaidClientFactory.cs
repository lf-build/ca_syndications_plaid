﻿using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Plaid.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Plaid
{
    public class PlaidClientFactory : IPlaidClientFactory
    {
        public PlaidClientFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IPlaidClient Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();
            return new PlaidClient(logger, configuration);
        }
    }
}
