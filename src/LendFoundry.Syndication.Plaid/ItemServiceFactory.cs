﻿using LendFoundry.Syndication.Plaid.Abstractions;
using LendFoundry.Syndication.Plaid.Persistence;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Security.Tokens;
using System;
using CapitalAlliance.Cashflow.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.EventHub;
#else
using Microsoft.Framework.DependencyInjection;
using LendFoundry.EventHub.Client;
#endif

namespace LendFoundry.Syndication.Plaid
{
    public class ItemServiceFactory : IItemServiceFactory
    {
        public ItemServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IItemService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        { 
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var repositoryFactory = Provider.GetService<IRepositoryFactory>();
            var itemRepository = repositoryFactory.CreateItemRepository(reader);
            var clientTrackRepository = repositoryFactory.CreateClientTrackRepository(reader);
            var transactionRepository = repositoryFactory.CreateTransactionRepository(reader);
            var institutionRepository = repositoryFactory.CreateInstitutionsRepository(reader);          

            var tokenReader = Provider.GetService<ITokenReader>();
            var tokenParser = Provider.GetService<ITokenHandler>();

            var plaidClientServiceFactory = Provider.GetService<IPlaidClientFactory>();
            var plaidClientService = plaidClientServiceFactory.Create(reader, handler, logger);

            var webhookRepository = repositoryFactory.CreateWebhookRepository(reader);
            var plaidDelinkRepository = repositoryFactory.CreatePlaidDelinkRepository(reader);

            var lookUpFactory = Provider.GetService<ILookupClientFactory>();
            var lookUpService = lookUpFactory.Create(reader);
            var decisionEngineFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngine = decisionEngineFactory.Create(reader);

            var cashflowClientFactory = Provider.GetService<ICashflowClientFactory>();
            var cashflowService = cashflowClientFactory.Create(reader);

            var eventHubServiceFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHubService = eventHubServiceFactory.Create(reader);         


            return new ItemService(
                logger, 
                plaidClientService, 
                tenantTime,
                configuration, 
                institutionRepository, 
                itemRepository, 
                transactionRepository, 
                webhookRepository, 
                plaidDelinkRepository,
                tokenReader, 
                tokenParser,
                eventHubService,               
                decisionEngine,
                lookUpService,
                cashflowService,             
                clientTrackRepository
                );
            

        }
    }
}
