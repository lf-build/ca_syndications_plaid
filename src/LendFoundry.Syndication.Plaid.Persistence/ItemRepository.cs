﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using System.Linq;
using System;
using LendFoundry.Syndication.Plaid.Abstractions;

namespace LendFoundry.Syndication.Plaid.Persistence
{
    /// <summary>
    /// ItemRepository
    /// </summary>
    public class ItemRepository : MongoRepository<IItem, Item>, IItemRepository
    {
        /// <summary>
        /// Maps the collection properties.
        /// </summary>
        static ItemRepository()
        {
            BsonClassMap.RegisterClassMap<Item>(map =>
            {
                map.AutoMap();
                map.MapProperty(item => item.EntityId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.ItemId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.InstitutionId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.PublicToken).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.AccessToken).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.CreatedDateTime).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.Accounts).SetIgnoreIfDefault(true);
                var type = typeof(Item);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<Balances>(map =>
            {
                map.AutoMap();
                var type = typeof(Balances);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemRepository"/> class.
        /// </summary>
        /// <param name="tenantService">tenantService</param>
        /// <param name="configuration">configuration</param>
        public ItemRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "item")
        {
            CreateIndexIfNotExists("tenant_id", Builders<IItem>.IndexKeys
                .Ascending(item => item.TenantId), false);

            CreateIndexIfNotExists("applicant_id", Builders<IItem>.IndexKeys
                .Ascending(item => item.TenantId)
                .Ascending(item => item.EntityId), false);

            CreateIndexIfNotExists("plaid_item_id", Builders<IItem>.IndexKeys
                .Ascending(item => item.TenantId)
                .Ascending(item => item.ItemId), false);
        }

        /// <summary>
        /// GetItemByApplicantId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>Item Collection</returns>
        public IList<IItem> GetItemsByApplicantId(string applicant_Id)
        {
            var delItemList = new List<IItem>();

            var itemList = Query
                            .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                                   && item.EntityId.Equals(applicant_Id))
                            .ToList<IItem>();

            //// Filter data set so not to display accounts marked as isLinked
            foreach (var item in itemList)
            {
                //// Hide all accounts which are marked as IsLinked = false 
                item.Accounts.RemoveAll(account => account.IsLinked == false);

                //// Dont display items which doesn't have any accounts 
                if (item.Accounts.Count == 0)
                    delItemList.Add(item);

            }

            foreach (var delItem in delItemList)
                itemList.Remove(delItem);

            return itemList;
        }

        /// <summary>
        /// GetAccountsInfoByItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>accounts</returns>
        public IList<Accounts> GetAccountsInfoByItemId(string itemId)
        {
            var accountList = new List<Accounts>();

            Query
                .Where(account => account.TenantId.Equals(TenantService.Current.Id)
                        && account.ItemId.Equals(itemId))
                .SingleOrDefault<IItem>()
                .Accounts
                    .ForEach(account =>
                    {
                        if (account.IsLinked)
                        {
                            accountList.Add(account);
                        }
                    });

            return accountList;
        }

        /// <summary>
        /// GetApplicantAllAccessToken
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns>All access token</returns>
        public IList<string> GetApplicantAllAccessToken(string applicantId)
        {
            return Query
                    .Where(item => item.TenantId == TenantService.Current.Id
                           && item.EntityId == applicantId && item.AccessToken != null)
                    .Select(item => item.AccessToken)
                    .ToList<string>();
        }

        /// <summary>
        /// GetAccountsByItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>All accounts</returns>
        public IEnumerable<string> GetAccountsByItemId(string itemId)
        {
            List<string> strAccounts = new List<string>();
            Query
                .Where(item => item.TenantId == TenantService.Current.Id && item.ItemId == itemId)
                .FirstOrDefault<IItem>().Accounts.ForEach(account =>
                {
                    if (account.IsLinked)
                        strAccounts.Add(account.PlaidAccountId);
                });

            return strAccounts;
        }

        /// <summary>
        /// GetAccountsByApplicantId
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns>All accounts</returns>
        public IList<string> GetAccountsByApplicantId(string applicantId)
        {
            var accountStringList = new List<string>();
            Collection.Find(item => item.EntityId.Equals(applicantId))
                .ToList<IItem>().ForEach(
                    item => item.Accounts
                    .ForEach(account => { if (account.IsLinked) { accountStringList.Add(account.PlaidAccountId); } })
                );


            return accountStringList;
        }

        /// <summary>
        /// GetAccessTokenByItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>access token</returns>
        public string GetAccessTokenByItemId(string itemId)
        {
            return Query
                .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                        && item.ItemId.Equals(itemId))
                .Select(item => item.AccessToken)
                .FirstOrDefault<string>();
        }

        /// <summary>
        /// GetItemStatus
        /// </summary>
        /// <param name="applicant_Id">applicantId</param>
        /// <param name="institution_Id">institutionId</param>
        /// <returns>-1 if item not exists, 1 if item and account exists, 0 if item exist but all accounts unlinked</returns>
        public ItemStatus GetItemStatus(string applicant_Id, string institution_Id)
        {
            var objItem = Query
                            .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                                   && item.EntityId.Equals(applicant_Id)
                                   && item.InstitutionId.Equals(institution_Id))
                            .FirstOrDefault<IItem>();

            if (objItem == null)
            {
                return ItemStatus.NoRecordsFound; //// No Records Found
            }
            objItem.Accounts.RemoveAll(account => account.IsLinked == false);

            if (objItem.Accounts.Count > 0)
            {
                return ItemStatus.AccountExists;
            }

            return ItemStatus.AccountNotExists;
        }

        /// <summary>
        /// GetItemIdByAccessToken
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <returns>Item id</returns>
        public string GetItemIdByAccessToken(string accessToken)
        {
            return Query
                .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                        && item.AccessToken.Equals(accessToken))
                .Select(item => item.ItemId)
                .FirstOrDefault<string>();
        }

        /// <summary>
        /// Get Item details given applicant_Id and institution_Id
        /// </summary>
        /// <param name="applicant_Id">applicantId</param>
        /// <param name="institution_Id">institutionId</param>
        /// <returns>Item Data</returns>
        public IItem GetItemGivenApplicantIdInstitutionId(string applicant_Id, string institution_Id)
        {
            return Query
                    .Where(item => item.TenantId == TenantService.Current.Id
                            && item.EntityId == applicant_Id
                            && item.InstitutionId == institution_Id
                            && item.AccessToken != null)
                    .FirstOrDefault<IItem>();
        }

        /// <summary>
        /// GetItemByAccountId
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <returns>return Item</returns>
        public IItem GetItemByAccountId(string accountId)
        {
            var item = Collection
                        .Find(Builders<IItem>
                              .Filter
                              .Eq("Accounts.PlaidAccountId", accountId))
                        .FirstOrDefault<IItem>();

            return item;
        }


        /// <summary>
        /// Get Item details given item_id
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>Item Data</returns>
        public IItem GetItemByGivenItemId(string item_id)
        {
            var result = Query
                    .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                            && item.ItemId.Equals(item_id))
                    .FirstOrDefault<IItem>();

            return result;
        }

        /// <summary>
        /// Add a new item
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>string</returns>
        public string AddItem(IItem item)
        {
            //// Update TenantId 
            item.TenantId = TenantService.Current.Id;

            //// Store item in database 
            Collection
                .InsertOne(item);

            return "Item added successfully.";
        }

        /// <summary>
        /// Hard Delete Item Information given itemId
        /// </summary>
        /// <param name="item_id">itemId</param>
        /// <returns>true or false</returns>
        public bool DeleteItemGivenItemId(string item_id)
        {
            var result = Collection.DeleteOne(Builders<IItem>
                                                .Filter
                                                .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                                                        && item.ItemId.Equals(item_id)));

            //TODO: Delete transaction information 

            return result.IsAcknowledged;
        }

        /// <summary>
        /// Hard Delete Item Information given accessToken
        /// </summary>
        /// <param name="accessToken">accessToken</param>
        /// <returns>true or false</returns>
        public bool DeleteItemGivenAccessToken(string accessToken)
        {
            var result = Collection
                            .DeleteOne(Builders<IItem>
                                        .Filter
                                        .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                                                && item.AccessToken.Equals(accessToken)));
            // TODO: Delete transaction information 

            return result.IsAcknowledged;
        }

        /// <summary>
        /// RemoveAccountByAccountId
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <param name="accountId">accountId</param>
        /// <returns>true or false</returns>
        public bool RemoveAccountByAccountId(string applicantId, string accountId)
        {
            //// Get the list of items linked for given applicant
            var objItem = Collection
                            .Find(Builders<IItem>
                                  .Filter
                                  .Eq("Accounts.PlaidAccountId", accountId))
                            .FirstOrDefault<IItem>();

            if (objItem == null)
                throw new NotFoundException($"Item {objItem} cannot be found");

            //// locate the account using accountId and update the balances
            var accounts = objItem.Accounts;
            var accountToUpdate = accounts.Find(account => account.PlaidAccountId == accountId);

            if (accountToUpdate == null)
                throw new NotFoundException($"Account {accountToUpdate} not found");

            accountToUpdate.IsLinked = false;

            //// Update information in database
            UpdateResult updateResult = Collection.UpdateOne
            (
                Builders<IItem>.Filter
                    .Where(item => item.TenantId == TenantService.Current.Id &&
                                item.EntityId == applicantId && item.InstitutionId == objItem.InstitutionId),
                Builders<IItem>.Update.Set(a => a.Accounts, accounts)
            );

            return updateResult.IsAcknowledged;
        }

        /// <summary>
        /// Update Item
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>string</returns>
        public void UpdateItem(IItem item)
        {
            var result = Collection
                            .UpdateOne(Builders<IItem>
                                        .Filter
                                        .Where(objItem => objItem.TenantId.Equals(TenantService.Current.Id)
                                                && objItem.Id.Equals(item.Id)),
                                       Builders<IItem>
                                        .Update
                                        .Set(objItem => objItem.ItemId, item.ItemId)
                                        .Set(objItem => objItem.InstitutionId, item.InstitutionId)
                                        .Set(objItem => objItem.PublicToken, item.PublicToken)
                                        .Set(objItem => objItem.AccessToken, item.AccessToken)
                                        .Set(objItem => objItem.Accounts, item.Accounts));

        }

        /// <summary>
        /// Update Account Balances
        /// </summary>
        /// <param name="applicantId">Applicant Id</param>
        /// <param name="accountId">Account Id</param>
        /// <param name="balance">Balances</param>
        /// <returns></returns>
        public bool UpdateAccountBalancesGivenAccountId(string applicantId, string accountId, Balances balance)
        {
            //// Get the list of items linked for given applicant
            var objItem = Collection
                              .Find(Builders<IItem>
                                    .Filter
                                    .Eq("Accounts.PlaidAccountId", accountId))
                              .FirstOrDefault<IItem>();

            if (objItem == null)
                throw new NotFoundException($"Item {objItem} cannot be found");

            //// locate the account using accountId and update the balances
            var accounts = objItem.Accounts;
            var accountToUpdate = accounts
                                    .Find(account => account.PlaidAccountId.Equals(accountId));

            if (accountToUpdate == null)
                throw new NotFoundException($"Account {accountToUpdate} not found");

            accountToUpdate.Balances = balance;

            //// Update information in database
            UpdateResult updateResult = Collection
                .UpdateOne
                (
                    Builders<IItem>
                        .Filter
                        .Where(item => item.TenantId.Equals(TenantService.Current.Id)
                                && item.EntityId.Equals(applicantId)
                                && item.InstitutionId.Equals(objItem.InstitutionId)),
                    Builders<IItem>
                        .Update
                        .Set(a => a.Accounts, accounts)
                );

            return updateResult.IsAcknowledged;
        }

        /// <summary>
        /// Get the list of access token after the specified no of days
        /// </summary>
        /// <param name="days">No Of days</param>
        /// <returns>list of access token</returns>
        public IList<string> GetAccessToken(int days)
        {
            DateTime checkDays = DateTime.UtcNow.AddDays(days * (-1));
            var items = Query
                        .Where(item => item.TenantId == TenantService.Current.Id
                               && !string.IsNullOrEmpty(item.AccessToken)
                               && item.CreatedDateTime.Time < checkDays.ToUniversalTime()).ToList();

            var accessTokenList = items.SelectMany(x => new List<string>()
                {
                    x.AccessToken
                }).ToList();
            return accessTokenList;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<FilterApplications> GetAccessTokenEntityId()
        {
            var EntityIdList = new List<FilterApplications>();

            var items = Query
                       .Where(item => item.TenantId == TenantService.Current.Id
                              && !string.IsNullOrEmpty(item.AccessToken)).ToList();
            foreach (var item in items)
            {
                var FilterApplications = new FilterApplications();
                FilterApplications.EntityType = item.EntityType;
                FilterApplications.EntiyId = item.EntityId;
                FilterApplications.ItemCreateOn = item.CreatedDateTime;
                FilterApplications.AccessToken = item.AccessToken;
                EntityIdList.Add(FilterApplications);
            }
            return EntityIdList;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="days"></param>
        /// <param name="entityIds"></param>
        /// <returns></returns>
        public IList<string> GetAccessToken(int days,List<string> entityIds)
        {
            DateTime checkDays = DateTime.UtcNow.AddDays(days * (-1));
            var items = Query
                        .Where(item => item.TenantId == TenantService.Current.Id
                               && !string.IsNullOrEmpty(item.AccessToken)
                               && entityIds.Contains(item.EntityId)
                               && item.CreatedDateTime.Time < checkDays.ToUniversalTime()).ToList();

            var accessTokenList = items.SelectMany(x => new List<string>()
                {
                    x.AccessToken
                }).ToList();
            return accessTokenList;
        }
        /// <summary>
        /// UpdateAllItemsBeforeThreeMonths
        /// </summary>
        /// <param name="noOfDaysBefore">noOfDaysBefore</param>
        /// <returns>true or false</returns>
        public void UpdateAccessToNull(List<string> accessTokens)
        {
            accessTokens.ForEach(at =>
                Collection
                     .UpdateOne(Builders<IItem>
                                 .Filter
                                 .Where(objItem => objItem.TenantId.Equals(TenantService.Current.Id)
                                         && objItem.AccessToken.Contains(at)),
                                 Builders<IItem>
                                 .Update
                                 .Set(objItem => objItem.AccessToken, null))
            );
        }
    }
}
