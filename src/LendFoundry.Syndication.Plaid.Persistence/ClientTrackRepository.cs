﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using System.Linq;
using System;
using LendFoundry.Syndication.Plaid.Abstractions;

namespace LendFoundry.Syndication.Plaid.Persistence
{
    /// <summary>
    /// ItemRepository
    /// </summary>
    public class ClientTrackRepository : MongoRepository<IClientTrack, ClientTrack>, IClientTrackRepository
    {
        /// <summary>
        /// Maps the collection properties.
        /// </summary>
        static ClientTrackRepository()
        {
            BsonClassMap.RegisterClassMap<ClientTrack>(map =>
            {
                map.AutoMap();
                map.MapProperty(item => item.EntityId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.EntityType).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.Institutions).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.LinkRequestId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.LinkSessionId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.CreatedDate).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.PlaidApiRequestId).SetIgnoreIfDefault(true);
                map.MapProperty(item => item.Status).SetIgnoreIfDefault(true);
                var type = typeof(ClientTrack);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemRepository"/> class.
        /// </summary>
        /// <param name="tenantService">tenantService</param>
        /// <param name="configuration">configuration</param>
        public ClientTrackRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "client-track")
        {

        }

       
        /// <summary>
        /// Add a new item
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>string</returns>
        public string AddItem(IClientTrack item)
        {
            //// Update TenantId 
            item.TenantId = TenantService.Current.Id;

            //// Store item in database 
            Collection
                .InsertOne(item);

            return "ClientTrack added successfully.";
        }        
    }
}
