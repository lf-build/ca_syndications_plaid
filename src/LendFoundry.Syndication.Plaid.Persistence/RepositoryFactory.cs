﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using LendFoundry.Syndication.Plaid.Abstractions;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Syndication.Plaid.Persistence
{
    public interface IRepositoryFactory
    {
        IItemRepository CreateItemRepository(ITokenReader reader);

        ITransactionRepository CreateTransactionRepository(ITokenReader reader);

        IInstitutionsRepository CreateInstitutionsRepository(ITokenReader reader);

        IWebhookRepository CreateWebhookRepository(ITokenReader reader);

        IPlaidDelinkRepository CreatePlaidDelinkRepository(ITokenReader reader);
        IClientTrackRepository CreateClientTrackRepository(ITokenReader reader);
    }

    public class RepositoryFactory : IRepositoryFactory
    {
        public RepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IItemRepository CreateItemRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new ItemRepository(tenantService, mongoConfiguration);
        }

        public ITransactionRepository CreateTransactionRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new TransactionRepository(tenantService, mongoConfiguration);
        }

        public IInstitutionsRepository CreateInstitutionsRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new InstitutionsRepository(tenantService, mongoConfiguration);
        }

        public IWebhookRepository CreateWebhookRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new WebhookRepository(tenantService, mongoConfiguration);
        }

        public IPlaidDelinkRepository CreatePlaidDelinkRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new PlaidDelinkRepository(tenantService, mongoConfiguration);
        }
        public IClientTrackRepository CreateClientTrackRepository(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new ClientTrackRepository(tenantService, mongoConfiguration);
        }
    }
}
